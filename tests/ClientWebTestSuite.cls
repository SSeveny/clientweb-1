 
 /*------------------------------------------------------------------------
    File        : ClientWebTestSuite
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : chrisr
    Created     : Wed Jun 05 10:37:27 EDT 2019
    Notes       : 
  ----------------------------------------------------------------------*/

using com.sit.testing.base.TestSuiteBase.

block-level on error undo, throw.

class ClientWebTestSuite inherits TestSuiteBase: 

    method public override void Initialize():
    end method.
    
    method public override void Deinitialize():
    end method.

end class.