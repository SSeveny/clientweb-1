
 /*------------------------------------------------------------------------
    File        : ClientLookupsOBPWebHandler
    Purpose     : Generic web handler to handle getting the client lookup tables
    Syntax      :
    Description :
    Author(s)   : lauries
    Created     : Jun 26, 2018
    Notes       :
  ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.*.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.serializer.*.
using com.sit.CommonMessageLogger.
using com.sit.errorstatus.*.
using com.sit.obp.model.PPlusSubSystemCodes.
using com.sit.obp.api.ChecklistTypeWorkflow.

block-level on error undo, throw.

class com.sit.obp.web.ClientLookupsOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    define private property Site as character no-undo
        get():
            if this-object:Site = "":U then
                this-object:Site = dynamic-function( "getSessionProperty":U, "SiteCode":U ).

            return this-object:Site.
        end get.
        set.

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( poRequest as IWebRequest ):
        return "GET":U.
    end method.

    /*------------------------------------------------------------------------------
    Purpose: Handle query request for client management system lookups.
    Description:
    Notes:
    @param poRequest - the incoming web request
    @return Http status number
    ------------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse           as OpenEdge.Net.HTTP.IHttpResponse          no-undo.
        define variable oJson                   as JsonArray                                no-undo.
        define variable iSystemInPath           as integer                                  no-undo.
        define variable cLookupType             as character                                no-undo.
        define variable cQueryParamVal          as character initial ?                      no-undo.
        define variable lQueryParamVal          as logical initial ?                        no-undo.
        define variable oWorkflow               as com.sit.obp.api.base.LookupWorkflow      no-undo.
        define variable oChecklistTypeWorkflow  as com.sit.obp.api.ChecklistTypeWorkflow    no-undo.
        define variable oClientGenderWorkflow   as com.sit.obp.api.ClientGenderWorkflow     no-undo.
        define variable oPPlusSubSystemCodes    as com.sit.obp.model.PPlusSubSystemCodes    no-undo.
        define variable oClientCorrespondenceWorkflow as com.sit.obp.api.ClientCorrespondenceTypeWorkflow no-undo.
        define variable cChecklistTypePathParam as character                                no-undo.
        define variable cRequiredParamValue     as character extent                         no-undo.
        define variable cOptionalParamValue     as character extent                         no-undo.
        define variable oParamError             as logical                                  no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        do on error undo, throw:
            iSystemInPath = lookup("System":U, poRequest:URI:Path, '/').
            if iSystemInPath > 0 then
                /* extract the lookup type from the path...will be the part right after the 'System' */
                cLookupType = entry( iSystemInPath + 1, poRequest:URI:Path, '/' ).
            else
                undo, throw new com.sit.obp.exceptions.ResourceNotFoundException(substitute("Unknown Resource: &1":T,poRequest:URI:Path)).

            do on error undo, throw:
                assign extent(cRequiredParamValue) = 1
                       cRequiredParamValue[1] = substitute("LookupType,&1":U,cLookupType)
                       .
                this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            	assign extent(cRequiredParamValue) = ?.

            	catch oBRError as com.sit.obp.exceptions.BusinessRuleException :
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                    oParamError = true.
                end catch.
        	end.

        	if oParamError = false then do
        	on error undo, throw:
                case cLookupType:
                    when "Titles" then
                    do:
                        assign oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientTitleWorkflow":U),
                                                com.sit.obp.api.ClientTitleWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oWorkflow:Query( input cQueryParamVal ).
                    end.

                    when "MaritalStatuses" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "MaritalStatusCode", poRequest )
                               oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientMaritalStatusWorkflow":U),
                                                com.sit.obp.api.ClientMaritalStatusWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oWorkflow:Query( input cQueryParamVal ).
                    end.

                    when "OccupancyTypes" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "CIFType", poRequest )
                               oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientOccupancyTypeWorkflow":U),
                                                com.sit.obp.api.ClientOccupancyTypeWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oWorkflow:Query( input cQueryParamVal ).
                    end.

                    when "Occupations" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "OccupationCode", poRequest )
                               oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientOccupationWorkflow":U),
                                                com.sit.obp.api.ClientOccupationWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oWorkflow:Query( input cQueryParamVal ) .
                    end.

                    when "IncomeTypes" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "IncomeTypeCode", poRequest )
                               oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientIncomeTypeWorkflow":U),
                                                com.sit.obp.api.ClientIncomeTypeWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oWorkflow:Query( input cQueryParamVal ).
                    end.

                    when "AssetTypes" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "AssetTypeCode", poRequest )
                               oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientAssetTypeWorkflow":U),
                                                com.sit.obp.api.ClientAssetTypeWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oWorkflow:Query( input cQueryParamVal ).
                    end.

                    when "LiabilityTypes" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "LiabilityTypeCode", poRequest )
                               oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientLiabilityTypeWorkflow":U),
                                                com.sit.obp.api.ClientLiabilityTypeWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oWorkflow:Query( input cQueryParamVal ).
                    end.

                    when "CommunicationTypes" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "CommunicationTypeCode", poRequest )
                               oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientCommunicationTypeWorkflow":U),
                                                com.sit.obp.api.ClientCommunicationTypeWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oWorkflow:Query( input cQueryParamVal ).
                    end.

                    when "MarketingMaterialTypes" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "MarketingMaterialTypeCode", poRequest )
                               oClientCorrespondenceWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientCorrespondenceTypeWorkflow":U),
                                                                    com.sit.obp.api.ClientCorrespondenceTypeWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oClientCorrespondenceWorkflow:Query( input cQueryParamVal, this-object:Site ).
                    end.

                    when "MarketResearchTypes" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "MarketResearchTypeCode", poRequest )
                               oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientMarketResearchTypeWorkflow":U),
                                                com.sit.obp.api.ClientMarketResearchTypeWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                             oJson = oWorkflow:Query( input cQueryParamVal ).
                    end.

                    when "AddressTypes" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "AddressTypeCode", poRequest )
                               oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientAddressTypeWorkflow":U),
                                                com.sit.obp.api.ClientAddressTypeWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oWorkflow:Query( input cQueryParamVal ).
                    end.

                    when "ChecklistTypes" then do:
                        assign lQueryParamVal = com.sit.obp.web.OBPWebHandler:GetLogicalQueryParamValue( "AlertFlag", poRequest )
                               cChecklistTypePathParam = poRequest:GetPathParameter( "ChecklistType":U )
                               oChecklistTypeWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ChecklistTypeWorkflow":U),
                                                             com.sit.obp.api.ChecklistTypeWorkflow)
                               oPPlusSubSystemCodes = cast( com.sit.obp.ObjectFactory:New( get-class(com.sit.obp.model.PPlusSubSystemCodes )),
                                                            com.sit.obp.model.PPlusSubSystemCodes )
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("ChecklistType,&1":U,cChecklistTypePathParam).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                        do:
                            if cChecklistTypePathParam > "":U then
                                oJson = oChecklistTypeWorkflow:GetChecklistDefaultTexts( input "", input oPPlusSubSystemCodes:ClientManagement, input cChecklistTypePathParam ).
                            else
                                oJson = oChecklistTypeWorkflow:GetChecklistTypes( input "", input oPPlusSubSystemCodes:ClientManagement, input lQueryParamVal ).
                        end.
                    end.

                    when "IndustrySectors" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "IndustrySectorCode", poRequest )
                               oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientIndustrySectorWorkflow":U),
                                                com.sit.obp.api.ClientIndustrySectorWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oWorkflow:Query( input cQueryParamVal ).
                    end.

                    when "Genders" then
                    do:
                        assign cQueryParamVal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "GenderCode", poRequest )
                               lQueryParamVal = if cQueryParamVal = ? then true    /* GET the full list of gender types, including the blank value type: Non-Gender */
                                                else false    /* GET a specific gender type record */
                               oClientGenderWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientGenderWorkflow":U),
                                                            com.sit.obp.api.ClientGenderWorkflow)
                               extent(cOptionalParamValue) = 1
                               cOptionalParamValue[1] = substitute("QueryParamVal,&1":U,cQueryParamVal).

                        do on error undo, throw:
                            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                            catch oError as com.sit.obp.exceptions.BusinessRuleException :
                                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                                oParamError = true.
                            end catch.
                        end.

                        if oParamError = false then
                            oJson = oClientGenderWorkflow:GetGenderTypes( input cQueryParamVal, input lQueryParamVal ) .
                    end.

                    otherwise
                        /* send a error message back if we didn't get a Lookup Type we expected */
                        undo, throw new com.sit.obp.exceptions.BusinessRuleException(substitute("Unknown Resource Type: &1":T,cLookupType)).
                end case.

                if valid-object( oJson ) then
                    oHttpResponse:Entity = oJson.
                else
                    oHttpResponse:Entity = new JsonArray().

                catch oErr as Progress.Lang.Error:
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
                end catch.
            end.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

end class.
