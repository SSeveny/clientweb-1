 
 /*------------------------------------------------------------------------
    File        : ClientIncomesOBPWebHandler
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : shainep
    Created     : May 3, 2018
    Notes       : 
  ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.*.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.serializer.*.
using com.sit.obp.api.ClientIncome.

block-level on error undo, throw.

class com.sit.obp.web.ClientIncomesOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ), "Character":U) > 0 then do:
            if length(poRequest:GetPathParameter( "SequenceNumber":U ), "Character":U) > 0 then
                return "GET,PUT,DELETE":U.
            else
                return "GET,POST":U.
        end.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

    method override protected integer HandleGet(input poRequest as OpenEdge.Web.IWebRequest):
          
        define variable oHttpResponse       as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson               as JsonArray                       no-undo.
        define variable nClientPathParam    as integer                         no-undo.
        define variable nSequencePathParam  as integer                         no-undo.
        define variable oIncomeWorkflow     as ClientIncome                    no-undo.
        define variable cRequiredParamValue as character extent 1              no-undo.
        define variable cOptionalParamValue as character extent 1              no-undo.
        
        com.sit.CommonMessageLogger:OperationStart( this-object:className,"HandleGet":U ).
         
        assign 
            oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().
        
        com.sit.CommonMessageLogger:LogPathParameters(this-object:className,"HandleGet":U,"ClientNumber,SequenceNumber":U, poRequest).
        
        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cOptionalParamValue[1] = substitute("SequenceNumber,&1":U, poRequest:GetPathParameter( "SequenceNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            assign nClientPathParam = integer(entry(2,cRequiredParamValue[1]))
                   nSequencePathParam = integer(entry(2,cOptionalParamValue[1])).
                   
            if nClientPathParam > 0 then 
            do:
                oIncomeWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientIncome":U),com.sit.obp.api.ClientIncome).
                
                // GetPathParameter returns empty string when parameter is missing
                if entry(2,cOptionalParamValue[1]) > "":U then
                do: 
                    oJson = oIncomeWorkflow:Query(input nClientPathParam,input nSequencePathParam). /* Error may happen here? How to manage this? */
                
                    if valid-object(oJson) and oJson:length = 1 then
                        oHttpResponse:Entity = oJson:GetJsonObject(1).
                    else
                        oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
                end.
                else do: 
                    oJson = oIncomeWorkflow:Query(input nClientPathParam). /* Error may happen here? How to manage this? */
                    
                    if valid-object(oJson) then
                        oHttpResponse:Entity = oJson.
                    else
                        oHttpResponse:Entity = new JsonArray().
                end.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
            
            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.      
        end.
        
        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).
        
        this-object:setHttpResponse(oHttpResponse).
        
        com.sit.CommonMessageLogger:OperationEnd( this-object:className,"HandleGet":U ).
          
        /* A response of 0 means that this handler will build the entire response;
         a non-zero value is mapped to a static handler in the webapp's /static/error folder.
         The mappings are maintained in the webapps's WEB-INF/web.xml
         A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
         enumeration */
        return 0.
        
        catch oErr as Progress.Lang.Error :
            
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).
                
            com.sit.CommonMessageLogger:OperationException( "com.sit.obp.web.ClientIncomesOBPWebHandler":U,"HandleGet":U,new com.sit.exceptions.AppError( oErr )).
              
        end catch.
    end method.
    
    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
      
        define variable oHttpResponse       as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson               as JsonObject                      no-undo.
        define variable nClientPathParam    as integer                         no-undo.
        define variable oIncomeWorkflow     as ClientIncome                    no-undo.
        define variable cRequiredParamValue as character extent 1              no-undo.
        define variable cOptionalParamValue as character extent                no-undo.
        
        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U ).
      
        assign 
            oHttpResponse    = com.sit.obp.web.OBPWebHandler:GetDefaultResponse(). 
        
        com.sit.CommonMessageLogger:LogPathParameters( this-object:className,"HandlePost":U,"ClientNumber":U,poRequest ).
        
        do on error undo, throw: 
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            assign nClientPathParam = integer(entry(2,cRequiredParamValue[1])).
            
            if nClientPathParam > 0 then 
            do:
                oIncomeWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientIncome":U),com.sit.obp.api.ClientIncome).
                
                oJson = cast( poRequest:Entity, JsonObject ).
    
                if not oJson:Has( "ClientNumber":U ) then
                    oJson:Add( "ClientNumber":U, nClientPathParam ).
    
                oJson = oIncomeWorkflow:Create(input nClientPathParam, input oJson ).
    
                if valid-object( oJson ) then                
                    assign 
                        oHttpResponse:Entity = oJson
                        oHttpResponse:StatusCode = integer( StatusCodeEnum:Created )
                    .
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                
            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.  
        end.
        
        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        com.sit.CommonMessageLogger:OperationEnd( this-object:className, "HandlePost":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
          
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            com.sit.CommonMessageLogger:OperationException(this-object:className, "HandlePost":U, oErr ).
          
        end catch.
    end method.
    
    method override protected integer HandlePut( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse   as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson           as JsonObject no-undo.
        define variable nClientNumber   as integer no-undo.
        define variable nSequenceNumber as integer no-undo.
        define variable oIncomeWorkflow as ClientIncome no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        assign oHttpResponse     = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandlePut":U ).

        com.sit.CommonMessageLogger:LogPathParameters( this-object:className, "HandlePut":U, "ClientNumber,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            assign nClientNumber = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber = integer(entry(2,cRequiredParamValue[2])).
                   
            if nClientNumber > 0 then 
            do:
                oIncomeWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientIncome":U),com.sit.obp.api.ClientIncome).
                oJson = cast( poRequest:Entity, JsonObject ).
                if not oJson:Has( "ClientNumber":U ) then
                    oJson:Add( "ClientNumber":U, nClientNumber ).
                if not oJson:Has( "IncomeSequence":U ) then
                    oJson:Add( "IncomeSequence":U, nSequenceNumber ).
                oIncomeWorkflow:Replace(nClientNumber, nSequenceNumber, input oJson).
                oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
        
            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.  
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        com.sit.CommonMessageLogger:OperationEnd( this-object:className, "HandlePut":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            com.sit.CommonMessageLogger:OperationException( this-object:className, "HandlePut":U, new com.sit.exceptions.AppError( oErr ) ).
        end catch.
    end method.

    method override protected integer HandleDelete( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.

        define variable nClientNumber as integer no-undo.
        define variable nSequenceNumber as integer no-undo.
        define variable oIncomeWorkflow as ClientIncome no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandleDelete":U ).

        assign 
            oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        com.sit.CommonMessageLogger:LogPathParameters( this-object:className, "HandleDelete":U, "ClientNumber,SequenceNumber":U, poRequest ).

        do on error undo, throw: 
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            assign nClientNumber = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber = integer(entry(2,cRequiredParamValue[2])).
                   
            if nClientNumber > 0 then 
            do:
                oIncomeWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientIncome":U),com.sit.obp.api.ClientIncome).
                oIncomeWorkflow:Delete(input nClientNumber,
                                       input nSequenceNumber).
    
                /* We return a 204 here because the request provided has caused the successful deletion of the record.
                   This means that there's nothing to return to the client application.
                   Using HTTP 204 tells the client that this is the case. */
                oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
        
            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.     
        end.
        
        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandleDelete":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            com.sit.CommonMessageLogger:OperationException(this-object:className, "HandleDelete":U, oErr ).
        end catch.
    end method.
    
end class.