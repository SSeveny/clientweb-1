
 /*------------------------------------------------------------------------
    File        : ClientContactsOBPWebHandler
    Purpose     :
    Syntax      :
    Description :
    Author(s)   : lauries
    Created     : May 3, 2018
    Notes       :
  ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.*.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.serializer.*.
using com.sit.obp.api.ClientContactWorkflow.

block-level on error undo, throw.

class com.sit.obp.web.ClientContactsOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ),"Character":U) > 0 then do:
            if length(poRequest:GetPathParameter( "ContactType":U ),"Character":U) > 0 then
                return "GET,PUT,DELETE":U.
            else
                return "GET,POST":U.
        end.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse           as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson                   as JsonArray                       no-undo.

        define variable cContactTypePathParam   as character                       no-undo.
        define variable iCIFNumber              as integer                         no-undo.
        define variable oClientContactWorkflow  as ClientContactWorkflow           no-undo.
        define variable cRequiredParamValue as character extent 1                  no-undo.
        define variable cOptionalParamValue as character extent                    no-undo.

        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               cContactTypePathParam   = poRequest:GetPathParameter( "ContactType":U )
        .

        com.sit.CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber,ContactType":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            iCifNumber = integer(entry(2,cRequiredParamValue[1])).
            
            if iCifNumber > 0 then 
            do:
                oClientContactWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientContactWorkflow":U),
                                                  com.sit.obp.api.ClientContactWorkflow).
                
                oJson = oClientContactWorkflow:Query(input iCIFNumber, input cContactTypePathParam ).

                if valid-object( oJson ) then
                    oHttpResponse:Entity = oJson.
                else
                    oHttpResponse:Entity = new JsonArray().
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).
        
        com.sit.CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U).
        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).
            com.sit.CommonMessageLogger:OperationException( "com.sit.obp.web.ClientContactsOBPWebHandler":U,
                                                  "HandleGet":U,
                                                  new com.sit.exceptions.AppError( oErr ) ).
        end catch.
    end method.

    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse           as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson                   as JsonObject                      no-undo.
        define variable oResponse               as JsonObject                      no-undo.
        define variable iCIFNumber              as integer                         no-undo.
        define variable oClientContactWorkflow  as ClientContactWorkflow           no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent no-undo.
        
        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U).

        com.sit.CommonMessageLogger:LogPathParameters( this-object:className,
                         "HandlePost":U,
                         "ClientNumber":U,
                         poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            iCifNumber = integer(entry(2,cRequiredParamValue[1])).
            
            if iCifNumber > 0 then 
            do:
                oClientContactWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientContactWorkflow":U),
                                                  com.sit.obp.api.ClientContactWorkflow).
                                                  
                assign oJson = cast( poRequest:Entity, JsonObject ).

                if not oJson:Has( "ClientNumber":U ) then
                    oJson:Add( "ClientNumber":U, entry(2,cRequiredParamValue[1]) ).

                oResponse = oClientContactWorkflow:Create(input iCIFNumber, input oJson).
                if valid-object( oResponse ) then
                    assign oHttpResponse:Entity = oResponse
                           oHttpResponse:StatusCode = integer( StatusCodeEnum:Created )
                    .
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).
        
         com.sit.CommonMessageLogger:OperationEnd( "com.sit.obp.web.ClientContactsOBPWebHandler":U, "HandlePost":U).
        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            com.sit.CommonMessageLogger:OperationException( this-object:className,
                                                  "HandlePost":U,
                                                  new com.sit.exceptions.AppError( oErr ) ).
        end catch.
    end method.

    method override protected integer HandlePut( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse           as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson                   as JsonObject                      no-undo.
        define variable cContactTypePathParam   as character                       no-undo.
        define variable iCIFNumber              as integer                         no-undo.
        define variable oClientContactWorkflow  as ClientContactWorkflow           no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent no-undo.
        
        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               cContactTypePathParam   = poRequest:GetPathParameter( "ContactType":U )
        .

        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandlePut":U ).

        com.sit.CommonMessageLogger:LogPathParameters( this-object:className,
                         "HandlePut":U,
                         "ClientNumber,ContactType":U,
                         poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("ContactType,&1":U,cContactTypePathParam).
            
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            iCifNumber = integer(entry(2,cRequiredParamValue[1])).
                   
            if iCifNumber > 0 then 
            do:
                oClientContactWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientContactWorkflow":U),
                                                  com.sit.obp.api.ClientContactWorkflow).
                                                  
                assign oJson = cast( poRequest:Entity, JsonObject ).
                
                oClientContactWorkflow:Update(input iCIFNumber,
                                              input cContactTypePathParam,
                                              input oJson).
                oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        com.sit.CommonMessageLogger:OperationEnd( this-object:className, "HandlePut":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            com.sit.CommonMessageLogger:OperationException( this-object:className,
                                                  "HandlePut":U,
                                                  new com.sit.exceptions.AppError( oErr ) ).
        end catch.
    end method.

    method override protected integer HandleDelete( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable cContactTypePathParam   as character                       no-undo.
        define variable iCIFNumber              as integer                         no-undo.
        define variable oClientContactWorkflow  as ClientContactWorkflow           no-undo.
        define variable cRequiredParamValue as character extent 2                  no-undo.
        define variable cOptionalParamValue as character extent                    no-undo.

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               cContactTypePathParam   = poRequest:GetPathParameter( "ContactType":U )
        .

        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandleDelete":U ).

        com.sit.CommonMessageLogger:LogPathParameters( this-object:className,
                         "HandleDelete":U,
                         "ClientNumber,ContactType":U,
                         poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("ContactType,&1":U,cContactTypePathParam).
            
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            iCifNumber = integer(entry(2,cRequiredParamValue[1])).
            
            if iCifNumber > 0 then 
            do:
                oClientContactWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientContactWorkflow":U),
                                                  com.sit.obp.api.ClientContactWorkflow).
                oClientContactWorkflow:Delete(input iCIFNumber, input cContactTypePathParam).

                oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
            
            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        com.sit.CommonMessageLogger:OperationEnd( this-object:className, "HandleDelete":U ).
        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            com.sit.CommonMessageLogger:OperationException( this-object:className,
                                                  "HandleDelete":U,
                                                  new com.sit.exceptions.AppError( oErr ) ).
        end catch.
    end method.

end class.
