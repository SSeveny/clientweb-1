 
/*------------------------------------------------------------------------
   File        : ExternalIdOBPWebHandler
   Purpose     : Provide a web API that allows client applications to manage a cif's links to external systems and accounts
   Syntax      : 
   Description : 
   Author(s)   : chrisr
   Created     : Oct 10, 2020
   Notes       : 
 ----------------------------------------------------------------------*/
using OpenEdge.Web.IWebRequest from propath.
using OpenEdge.Net.HTTP.StatusCodeEnum from propath.
using OpenEdge.Net.HTTP.IHttpResponse from propath.
using Progress.Json.ObjectModel.JsonConstruct from propath.
using Progress.Json.ObjectModel.JsonObject from propath.
using Progress.Json.ObjectModel.JsonArray from propath.
using com.sit.obp.model.ExternalIdModel from propath.
using com.sit.obp.model.ApiModelFactory from propath.
using com.sit.obp.model.ApiOperation from propath.
using com.sit.CommonMessageLogger from propath.
using com.sit.obp.web.OBPWebHandler from propath.
using com.sit.obp.web.OBPDefaultWebResponse from propath.
using com.sit.obp.ObjectFactory from propath.
using com.sit.obp.api.ExternalIdWorkflow from propath.
using com.sit.obp.exceptions.BusinessRuleException from propath.

block-level on error undo, throw.

class com.sit.obp.web.ExternalIdOBPWebHandler inherits OBPWebHandler:

   /*------------------------------------------------------------------------------
   Purpose: Return a list of HTTP mehthods which are supported by this web handler
   Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
   ------------------------------------------------------------------------------*/
   method override public character SupportedHTTPMethods( poRequest as IWebRequest ):
       if poRequest:GetPathParameter( "ClientNumber" ) > "" then do:
           if poRequest:GetPathParameter( "IdType" ) > "" then do:
               if poRequest:GetPathParameter( "SequenceNumber" ) > "" then
                   return "GET,PATCH,DELETE".
               else
                   return "GET".
           end.
           else
               return "GET,POST".
       end.

       /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
       undo, throw new com.sit.exceptions.ArgumentException().
   end method.

   /*------------------------------------------------------------------------------
   Purpose: Retrieve one or more, depending on request parameters, external id records
   Description:
   Notes:
   @param poRequest - the incoming web request
   @return Http status number
   ------------------------------------------------------------------------------*/
   method override protected integer HandleGet( input poRequest as IWebRequest ):
       define variable oHttpResponse as IHttpResponse no-undo.
       define variable oBody as JsonConstruct no-undo.
       define variable oWorkflow as ExternalIdWorkflow no-undo.
       define variable cClientNumberPathParameter as character no-undo.
       define variable nClientNumberPathParameter as int64 no-undo.
       define variable cIdTypePathParameter as character no-undo.
       define variable nSequenceNumberPathParameter as integer no-undo.
       define variable cRequiredParamValue as character extent 1 no-undo.
       define variable cOptionalParamValue as character extent 2 no-undo.

       CommonMessageLogger:OperationStart( this-object:className, "HandleGet" ).

       assign oHttpResponse = OBPWebHandler:GetDefaultResponse()
              cClientNumberPathParameter = poRequest:GetPathParameter( "ClientNumber" )
              oWorkflow = cast( ObjectFactory:New( get-class( ExternalIdWorkflow ) ), ExternalIdWorkflow )
       .

       CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet", "ClientNumber,IdType,SequenceNumber", poRequest ).

       do on error undo, throw:
           assign nClientNumberPathParameter = int64( cClientNumberPathParameter )
                  cIdTypePathParameter = poRequest:GetPathParameter( "IdType" )
                  nSequenceNumberPathParameter = integer( poRequest:GetPathParameter( "SequenceNumber" ) )
                  
                  cRequiredParamValue[1] = substitute( "ClientNumber,&1", nClientNumberPathParameter )
                  cOptionalParamValue[1] = substitute( "IdType,&1", cIdTypePathParameter )
                  cOptionalParamValue[2] = substitute( "SequenceNumber,&1", nSequenceNumberPathParameter )
           .
           
           this-object:ValidateParamValue( input cRequiredParamValue, input cOptionalParamValue ).
           
           // If we have a sequence number, but no id type then the requested URL is not valid.
           if nSequenceNumberPathParameter > 0 and
              ( cIdTypePathParameter = ? or cIdTypePathParameter = "" ) then
               undo, throw new BusinessRuleException( "Invalid request, cannot request ExternalId for SequenceNumber without IdType" ).
           
           // If the given client number is less than 1, this will be an invalid request.
           if nClientNumberPathParameter > 0 then do on error undo, throw:
               // Figure out which combination of path parameters we have, and fetch data appropriately.
               if cIdTypePathParameter > "" then do:
                   if nSequenceNumberPathParameter > 0 then
                       oBody = oWorkflow:Query( nClientNumberPathParameter, cIdTypePathParameter, nSequenceNumberPathParameter ):JsonContent.
                   else
                       oBody = oWorkflow:Query( nClientNumberPathParameter, cIdTypePathParameter ):JsonContent.
               end.
               else
                   oBody = oWorkflow:Query( nClientNumberPathParameter ):JsonContent.
    
               /* If oBody is actually a JsonArray, could add a length check here as well */
               if valid-object( oBody ) then
                   oHttpResponse:Entity = oBody.
               else
                   oHttpResponse = OBPDefaultWebResponse:response404.
           end.
           else
               oHttpResponse = OBPDefaultWebResponse:response400.

           catch oErr as Progress.Lang.Error:
               oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
           end catch.
       end.

       OBPWebHandler:WriteResponse( oHttpResponse ).

       this-object:setHttpResponse(oHttpResponse).

       CommonMessageLogger:OperationEnd( this-object:className, "HandleGet" ).

       /* A response of 0 means that this handler will build the entire response;
          a non-zero value is mapped to a static handler in the webapp's /static/error folder.
          The mappings are maintained in the webapps's WEB-INF/web.xml
          A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
          enumeration */
       return 0.
   end method.

   /*------------------------------------------------------------------------------
   Purpose: Create a new ExternalId record
   Description:
   Notes:
   @param poRequest - the incoming web request
   @return Http status number
   ------------------------------------------------------------------------------*/
   method override protected integer HandlePost( input poRequest as IWebRequest ):
       define variable oHttpResponse as IHttpResponse no-undo.
       define variable oBody as JsonObject no-undo.
       define variable oExternalId as ExternalIdModel no-undo.
       define variable oWorkflow as ExternalIdWorkflow no-undo.
       define variable cClientNumberPathParameter as character no-undo.
       define variable nClientNumberPathParameter as int64 no-undo.
       define variable cRequiredParamValue as character extent 1 no-undo.
       define variable oExternalIdJson as JsonObject no-undo.

       CommonMessageLogger:OperationStart( this-object:className, "HandlePost" ).

       assign oHttpResponse = OBPWebHandler:GetDefaultResponse( StatusCodeEnum:Created )
              cClientNumberPathParameter = poRequest:GetPathParameter( "ClientNumber" )
              oWorkflow = cast( ObjectFactory:New( get-class( ExternalIdWorkflow ) ), ExternalIdWorkflow )
       .

       CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost", "ClientNumber", poRequest ).

       do on error undo, throw:
           assign nClientNumberPathParameter = int64( cClientNumberPathParameter )
                  
                  cRequiredParamValue[1] = substitute( "ClientNumber,&1", nClientNumberPathParameter )
           .
           
           this-object:ValidateParamValue( input cRequiredParamValue ).
           
           // If the given client number is less than 1, this will be an invalid request.
           if nClientNumberPathParameter > 0 then do on error undo, throw:
               // Add ClientNumber to the request, to be used when building the API input object.
               oExternalIdJson = cast( poRequest:Entity, JsonObject ).
               oExternalIdJson:Add( "ClientNumber", nClientNumberPathParameter ).

               assign oExternalId = cast( ApiModelFactory:NewInputModel( ApiOperation:Create,
                                                                         get-class( ExternalIdModel ),
                                                                         oExternalIdJson ),
                                          ExternalIdModel )

                      oBody = oWorkflow:Create( oExternalId ):JsonContent
               .
    
               if valid-object( oBody ) then
                   oHttpResponse:Entity = oBody.
               else
                   oHttpResponse = OBPDefaultWebResponse:response404.
           end.
           else
               oHttpResponse = OBPDefaultWebResponse:response400.

           catch oErr as Progress.Lang.Error:
               oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
           end catch.
       end.

       OBPWebHandler:WriteResponse( oHttpResponse ).

       this-object:setHttpResponse(oHttpResponse).

       CommonMessageLogger:OperationEnd( this-object:className, "HandlePost" ).

       /* A response of 0 means that this handler will build the entire response;
          a non-zero value is mapped to a static handler in the webapp's /static/error folder.
          The mappings are maintained in the webapps's WEB-INF/web.xml
          A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
          enumeration */
       return 0.
   end method.

   /*------------------------------------------------------------------------------
   Purpose: Allow updates of portions of an existing ExternalId
   Description:
   Notes:
   @param poRequest - the incoming web request
   @return Http status number
   ------------------------------------------------------------------------------*/
   method override protected integer HandlePatch( input poRequest as IWebRequest ):
       define variable oHttpResponse as IHttpResponse no-undo.
       define variable oBody as JsonObject no-undo.
       define variable oExternalId as ExternalIdModel no-undo.
       define variable oWorkflow as ExternalIdWorkflow no-undo.
       define variable cClientNumberPathParameter as character no-undo.
       define variable cTypePathParameter as character no-undo.
       define variable nClientNumberPathParameter as int64 no-undo.
       define variable nSequenceNumberPathParameter as integer no-undo.
       define variable cRequiredParamValue as character extent 3 no-undo.
       define variable oExternalIdJson as JsonObject no-undo.

       CommonMessageLogger:OperationStart( this-object:className, "HandlePatch" ).

       assign oHttpResponse = OBPWebHandler:GetDefaultResponse()
              cClientNumberPathParameter = poRequest:GetPathParameter( "ClientNumber" )
              cTypePathParameter = poRequest:GetPathParameter( "IdType" )
              oWorkflow = cast( ObjectFactory:New( get-class( ExternalIdWorkflow ) ), ExternalIdWorkflow )
       .

       CommonMessageLogger:LogPathParameters( this-object:className, "HandlePatch", "ClientNumber,IdType,SequenceNumber", poRequest ).

       do on error undo, throw:
           assign nClientNumberPathParameter = int64( cClientNumberPathParameter )
                  nSequenceNumberPathParameter = integer( poRequest:GetPathParameter( "SequenceNumber" ) )
                  
                  cRequiredParamValue[1] = substitute( "ClientNumber,&1", nClientNumberPathParameter )
                  cRequiredParamValue[2] = substitute( "IdType,&1", cTypePathParameter )
                  cRequiredParamValue[3] = substitute( "SequenceNumber,&1", nSequenceNumberPathParameter )
                  
                  oExternalIdJson = cast( poRequest:Entity, JsonObject )
           .
           
           this-object:ValidateParamValue( input cRequiredParamValue ).
           
           // This request should be considered invalid if we're missing any of the path parameters, or
           // the "Number" parameter in the request body, which is required for update.
           if nClientNumberPathParameter > 0 and
              cTypePathParameter > "" and
              nSequenceNumberPathParameter > 0 and
              oExternalIdJson:Has( "Number" ) then do on error undo, throw:
               assign oExternalId = cast( ApiModelFactory:NewInputModel( ApiOperation:Update,
                                                                         get-class( ExternalIdModel ),
                                                                         oExternalIdJson ),
                                          ExternalIdModel )

                      oBody = oWorkflow:Update( oExternalId ):JsonContent
               .
    
               if valid-object( oBody ) then
                   oHttpResponse:Entity = oBody.
               else
                   oHttpResponse = OBPDefaultWebResponse:response404.
           end.
           else
               oHttpResponse = OBPDefaultWebResponse:response400.

           catch oErr as Progress.Lang.Error:
               oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
           end catch.
       end.

       OBPWebHandler:WriteResponse( oHttpResponse ).

       this-object:setHttpResponse(oHttpResponse).

       CommonMessageLogger:OperationEnd( this-object:className, "HandlePatch" ).

       /* A response of 0 means that this handler will build the entire response;
          a non-zero value is mapped to a static handler in the webapp's /static/error folder.
          The mappings are maintained in the webapps's WEB-INF/web.xml
          A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
          enumeration */
       return 0.
   end method.

   /*------------------------------------------------------------------------------
   Purpose: Allow callers to delete ExternalId records from the system
   Description:
   Notes:
   @param poRequest - the incoming web request
   @return Http status number
   ------------------------------------------------------------------------------*/
   method override protected integer HandleDelete( input poRequest as IWebRequest ):
       define variable oHttpResponse as IHttpResponse no-undo.
       define variable oExternalIdJson as JsonObject no-undo.
       define variable oExternalId as ExternalIdModel no-undo.
       define variable oWorkflow as ExternalIdWorkflow no-undo.
       define variable cClientNumberPathParameter as character no-undo.
       define variable cTypePathParameter as character no-undo.
       define variable nClientNumberPathParameter as int64 no-undo.
       define variable nSequenceNumberPathParameter as integer no-undo.
       define variable cRequiredParamValue as character extent 3 no-undo.

       CommonMessageLogger:OperationStart( this-object:className, "HandleDelete" ).

       assign oHttpResponse = OBPWebHandler:GetDefaultResponse( StatusCodeEnum:NoContent )
              cClientNumberPathParameter = poRequest:GetPathParameter( "ClientNumber" )
              cTypePathParameter = poRequest:GetPathParameter( "IdType" )
              oWorkflow = cast( ObjectFactory:New( get-class( ExternalIdWorkflow ) ), ExternalIdWorkflow )
       .

       CommonMessageLogger:LogPathParameters( this-object:className, "HandleDelete", "ClientNumber,IdType,SequenceNumber", poRequest ).

       do on error undo, throw:
           assign nClientNumberPathParameter = int64( cClientNumberPathParameter )
                  nSequenceNumberPathParameter = integer( poRequest:GetPathParameter( "SequenceNumber" ) )
                  
                  cRequiredParamValue[1] = substitute( "ClientNumber,&1", nClientNumberPathParameter )
                  cRequiredParamValue[2] = substitute( "IdType,&1", cTypePathParameter )
                  cRequiredParamValue[3] = substitute( "SequenceNumber,&1", nSequenceNumberPathParameter )
           .
           
           this-object:ValidateParamValue( input cRequiredParamValue ).
           
           // If the given client number is less than 1, this will be an invalid request.
           if nClientNumberPathParameter > 0 and
              cTypePathParameter > "" and
              nSequenceNumberPathParameter > 0 then do on error undo, throw:
               oExternalIdJson = new JsonObject().
               
               oExternalIdJson:Add( "ClientNumber", nClientNumberPathParameter ).
               oExternalIdJson:Add( "Type", cTypePathParameter ).
               oExternalIdJson:Add( "SequenceNumber", nSequenceNumberPathParameter ).
               
               assign oExternalId = cast( ApiModelFactory:NewInputModel( ApiOperation:Delete,
                                                                         get-class( ExternalIdModel ),
                                                                         oExternalIdJson ),
                                          ExternalIdModel )
               .
               
               oWorkflow:Delete( oExternalId ).
           end.
           else
               oHttpResponse = OBPDefaultWebResponse:response400.

           catch oErr as Progress.Lang.Error:
               oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
           end catch.
       end.

       OBPWebHandler:WriteResponse( oHttpResponse ).

       this-object:setHttpResponse(oHttpResponse).

       CommonMessageLogger:OperationEnd( this-object:className, "HandleDelete" ).

       /* A response of 0 means that this handler will build the entire response;
          a non-zero value is mapped to a static handler in the webapp's /static/error folder.
          The mappings are maintained in the webapps's WEB-INF/web.xml
          A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
          enumeration */
       return 0.
   end method.

end class.
