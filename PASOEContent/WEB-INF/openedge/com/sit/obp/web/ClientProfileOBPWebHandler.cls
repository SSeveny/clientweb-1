 /*------------------------------------------------------------------------
    File        : ClientProfileOBPWebHandler
    Purpose     : Called by PAS to handle client's profile reqeust for CRUD operations
    Syntax      :
    Description :
    Author(s)   : danielc
    Created     : June 1, 2018
    Notes       :
  ----------------------------------------------------------------------*/

using OpenEdge.Web.IWebRequest from propath.
using OpenEdge.Net.HTTP.StatusCodeEnum from propath.
using Progress.Json.ObjectModel.JsonArray from propath.
using Progress.Json.ObjectModel.JsonObject from propath.
using com.sit.obp.api.ClientProfileWorkflow from propath.
using com.sit.obp.ObjectFactory from propath.
using com.sit.CommonMessageLogger from propath.
using com.sit.obp.web.OBPWebHandler from propath.
using com.sit.obp.values.ClientProfileSearchValues from propath.
using com.sit.obp.model.ClientProfileModel from propath.

block-level on error undo, throw.

class com.sit.obp.web.ClientProfileOBPWebHandler inherits OBPWebHandler:

    define private property Site as character no-undo
       get():
            if this-object:Site = "" then
                this-object:Site = dynamic-function( "getSessionProperty", "SiteCode" ).

            return this-object:Site.
        end get.
        set.

    define private property Company as character no-undo
       get():
            if this-object:Company = "" then
                this-object:Company = dynamic-function( "getSessionProperty", "CompanyCode" ).

            return this-object:Company.
        end get.
        set.

    define private property clientProfileModel as com.sit.obp.model.ClientProfileModel no-undo
        get.
        set.

    define private property clientProfileSearchValues as com.sit.obp.values.ClientProfileSearchValues no-undo
        get.
        set.

    constructor public ClientProfileOBPWebHandler():
        assign this-object:clientProfileModel = cast( ObjectFactory:New( get-class(ClientProfileModel) ), ClientProfileModel )
               this-object:clientProfileSearchValues = cast( ObjectFactory:New( get-class(ClientProfileSearchValues) ), ClientProfileSearchValues)
        .
    end constructor.

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber" ), "Character") > 0 then
            return "GET,PUT".
        else
            return "GET,POST".
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle query request for client's profile
    Description:
    Notes:
    @param poRequest
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable oJsonArray as JsonArray no-undo.
        define variable nClientNumber as integer no-undo.
        define variable oQueryObject as JsonObject no-undo.
        define variable cQueryParameters as character extent no-undo.
        define variable cQueryParamKey as character no-undo.
        define variable cQueryParamValue as character no-undo.
        define variable i as integer no-undo.
        define variable oClientProfileWorkflow as ClientProfileWorkflow no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable oParamError as logical no-undo.
        define variable cRequiredParamValue as character extent no-undo.
        define variable cOptionalParamValue as character extent 1 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet" ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet", "ClientNumber", poRequest ).

        do on error undo, throw:
            assign cOptionalParamValue[1] = substitute("ClientNumber,&1",poRequest:GetPathParameter( "ClientNumber" )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            nClientNumber = integer(entry(2,cOptionalParamValue[1])).

            oParamList = new Progress.Lang.ParameterList(2).
            oParamList:SetParameter(1, "Character", "input", this-object:Company).
            oParamList:SetParameter(2, "Character", "input", this-object:Site).

            oClientProfileWorkflow =
                cast( ObjectFactory:New( "com.sit.obp.api.ClientProfileWorkflow" , oParamList),
                                         com.sit.obp.api.ClientProfileWorkflow ).

            if entry(2,cOptionalParamValue[1]) > "" then
            do:
                oJson = oClientProfileWorkflow:Query(input nClientNumber).

                if valid-object(oJson) then
                    assign oHttpResponse:Entity = oJson.
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
            end.
            else if poRequest:URI:GetQueryNames( output cQueryParameters ) > 0 then
            do:

                assign oQueryObject = new JsonObject().

                do i = 1 to extent( cQueryParameters ) on error undo, throw:

                    assign cQueryParamKey = cQueryParameters[ i ]
                           cQueryParamValue = OBPWebHandler:GetQueryParamValue( cQueryParamKey, poRequest ).

                    CommonMessageLogger:LogMessage( this-object:className,
                                                    "HandleGet",
                                                    substitute( "Query Parameter: &1 = &2", cQueryParamKey, cQueryParamValue ),
                                                    com.sit.logging.Logger:Extended ).

                    assign cOptionalParamValue[1] = substitute("&1,&2",cQueryParamKey, cQueryParamValue).

                    this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

                    case cQueryParamKey:
                        when this-object:clientProfileModel:LastName then
                            oQueryObject:Add( this-object:clientProfileModel:LastName, cQueryParamValue ).

                        when this-object:clientProfileSearchValues:LastNameMatchType then
                            oQueryObject:Add( this-object:clientProfileSearchValues:LastNameMatchType, cQueryParamValue ).

                        when this-object:clientProfileModel:FirstName then
                            oQueryObject:Add( this-object:clientProfileModel:FirstName, cQueryParamValue ).

                        when this-object:clientProfileSearchValues:FirstNameMatchType then
                            oQueryObject:Add( this-object:clientProfileSearchValues:FirstNameMatchType, cQueryParamValue ).

                        when this-object:clientProfileModel:DateOfBirth then
                            oQueryObject:Add( this-object:clientProfileModel:DateOfBirth, com.sit.util.DateType:AsDate( cQueryParamValue ):Value ).

                        when this-object:clientProfileModel:Entity then
                            oQueryObject:Add( this-object:clientProfileModel:Entity, cQueryParamValue ).

                        when this-object:clientProfileModel:PhoneNumber then
                            oQueryObject:Add( this-object:clientProfileModel:PhoneNumber, cQueryParamValue ).

                        when this-object:clientProfileModel:CellPhone then
                            oQueryObject:Add( this-object:clientProfileModel:CellPhone, cQueryParamValue ).

                        when this-object:clientProfileModel:SIN then
                            oQueryObject:Add( this-object:clientProfileModel:SIN, cQueryParamValue ).

                        when this-object:clientProfileModel:BIN then
                            oQueryObject:Add( this-object:clientProfileModel:BIN, cQueryParamValue ).

                        when this-object:clientProfileModel:ExternalReference then
                            oQueryObject:Add( this-object:clientProfileModel:ExternalReference, cQueryParamValue ).

                        when this-object:clientProfileModel:ClientStatus then
                            oQueryObject:Add( this-object:clientProfileModel:ClientStatus, cQueryParamValue ).

                        otherwise do:
                            // Account for query parameters that are used to support API pagination.
                            // These are part of the otherwise block because nothing in particular needs to be done if they're present.
                            if cQueryParamKey <> "size" and cQueryParamKey <> "page_start_id" then
                                undo, throw new com.sit.exceptions.AppError( substitute( "Unknown Query Parameter &1", cQueryParamKey ) ).
                        end.
                    end case.

                    catch oBRError as com.sit.obp.exceptions.BusinessRuleException :
                        oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
                        oParamError = true.
                        leave.
                    end catch.
                end.

                if not oParamError then do:
                    // Extract paging parameters, if any, from the request and provide them to the workflow.
                    oClientProfileWorkflow:SetPagingParameters( this-object:paginationInputValues:PageSize, this-object:paginationInputValues:PageStartId ).

                    oJsonArray = oClientProfileWorkflow:QueryList( oQueryObject ).
                end.

                if not valid-object( oJsonArray ) then
                    oJsonArray = new JsonArray().

                oHttpResponse:Entity = oJsonArray.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999", oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet", oErr ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle create request for client's profile
    Description:
    Notes:
    @param poRequest Json object contains client profile information to be created by the workflow
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable oClientProfileWorkflow as ClientProfileWorkflow no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost" ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost", "", poRequest ).

        do on error undo, throw:
            oParamList = new Progress.Lang.ParameterList(2).
            oParamList:SetParameter(1, "Character", "input", this-object:Company).
            oParamList:SetParameter(2, "Character", "input", this-object:Site).

            oClientProfileWorkflow =
                cast( ObjectFactory:New( "com.sit.obp.api.ClientProfileWorkflow" , oParamList),
                                         com.sit.obp.api.ClientProfileWorkflow ).

            oJson = oClientProfileWorkflow:Create(input cast( poRequest:Entity, JsonObject )).

            if valid-object(oJson) then
                assign oHttpResponse:Entity = oJson
                       oHttpResponse:StatusCode = integer( StatusCodeEnum:Created ).
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999", oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePost", oErr ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle replace request for client's profile
    Description:
    Notes:
    @param poRequest Json object contains client profile information to replace specified address by the workflow
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandlePut( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable nClientNumber as integer no-undo.
        define variable oClientProfileWorkflow as ClientProfileWorkflow no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePut" ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePut", "ClientNumber", poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1",poRequest:GetPathParameter( "ClientNumber" )).

            this-object:ValidateParamValue(input cRequiredParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(2).
                oParamList:SetParameter(1, "Character", "input", this-object:Company).
                oParamList:SetParameter(2, "Character", "input", this-object:Site).

                oClientProfileWorkflow =
                    cast( ObjectFactory:New( "com.sit.obp.api.ClientProfileWorkflow" , oParamList),
                                             com.sit.obp.api.ClientProfileWorkflow ).

                oJson = oClientProfileWorkflow:Replace(input nClientNumber,input cast( poRequest:Entity, JsonObject )).

                if valid-object(oJson) then
                    /* We return a 204 here because the data provided to this method has successfully overwritten the record.
                       This means that client application already has the current version of it, we don't need to send it back.
                       Using HTTP 204 tells the client that this is the case. */
                    oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePut" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999", oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePut", oErr ).
        end catch.
    end method.
end class.