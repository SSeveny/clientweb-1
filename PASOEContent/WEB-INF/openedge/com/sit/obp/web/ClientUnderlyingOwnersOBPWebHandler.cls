 /*------------------------------------------------------------------------
    File        : ClientUnderlyingOwnersOBPWebHandler
    Purpose     :
    Syntax      :
    Description :
    Author(s)   :
    Created     : May 31, 2018
    Notes       :
  ----------------------------------------------------------------------*/

using OpenEdge.Web.*.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.serializer.*.
using com.sit.CommonMessageLogger.

block-level on error undo, throw.

class com.sit.obp.web.ClientUnderlyingOwnersOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ), "Character":U) > 0 then do:
            if length(poRequest:GetPathParameter( "SequenceNumber":U ), "Character":U) > 0 then
                return "GET,PUT,DELETE":U.
            else
                return "GET,POST":U.
        end.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse      as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oBody              as JsonArray                       no-undo.
        define variable nClientNumber      as integer                         no-undo.
        define variable nOwnerClientNumber as integer                         no-undo.
        define variable nSequenceNumber    as integer                         no-undo.
        define variable oWorkflow as com.sit.obp.api.ClientUnderlyingOwnerWorkflow no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent 2 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign
            oHttpResponse      = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber,cOwnerClientNumber,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cOptionalParamValue[1] = substitute("SequenceNumber,&1":U, poRequest:GetPathParameter( "SequenceNumber":U ))
                   cOptionalParamValue[2] = substitute("OwnerClientNumber,&1":U, poRequest:GetPathParameter( "OwnerClientNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber    = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber  = integer(entry(2,cOptionalParamValue[1]))
                   nOwnerClientNumber = integer(entry(2,cOptionalParamValue[2])).

            if nClientNumber > 0 then 
            do:
                oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientUnderlyingOwnerWorkflow":U),
                                    com.sit.obp.api.ClientUnderlyingOwnerWorkflow).
                
                // GetPathParameter returns empty string when parameter is missing
                if nOwnerClientNumber > 0 and entry(2,cOptionalParamValue[1]) > "":U then
                do:
                    oBody = oWorkflow:Query( nClientNumber, nOwnerClientNumber, nSequenceNumber ).

                    if valid-object(oBody) and oBody:length = 1 then
                        oHttpResponse:Entity = oBody:GetJsonObject(1).
                    else
                        oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
                end.
                else do:
                    oBody = oWorkflow:Query( nClientNumber ).

                    if valid-object( oBody ) then
                        oHttpResponse:Entity = oBody.
                    else
                        oHttpResponse:Entity = new JsonArray().
                end.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oRequest      as JsonObject no-undo.
        define variable oResponse     as JsonObject no-undo.
        define variable nClientNumber as integer    no-undo.
        define variable oWorkflow as com.sit.obp.api.ClientUnderlyingOwnerWorkflow no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then 
            do:
                oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientUnderlyingOwnerWorkflow":U),
                                    com.sit.obp.api.ClientUnderlyingOwnerWorkflow).
                oRequest = cast( poRequest:Entity, JsonObject ).

                if not oRequest:Has( "ClientNumber":U ) then
                    oRequest:Add( "ClientNumber":U, string(nClientNumber) ).

                oResponse = oWorkflow:Create(input nClientNumber, input oRequest).
                if valid-object( oResponse ) then
                    assign oHttpResponse:Entity = oResponse
                           oHttpResponse:StatusCode = integer( StatusCodeEnum:Created )
                    .
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePost":U, oErr ).
        end catch.
    end method.

    method override protected integer HandlePut( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse      as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oRequest           as JsonObject                      no-undo.
        define variable nClientNumber      as integer                         no-undo.
        define variable nOwnerClientNumber as integer                         no-undo.
        define variable nSequenceNumber    as integer                         no-undo.
        define variable oWorkflow as com.sit.obp.api.ClientUnderlyingOwnerWorkflow no-undo.
        define variable cRequiredParamValue as character extent 3 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        assign
            oHttpResponse      = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandlePut":U ).

        com.sit.CommonMessageLogger:LogPathParameters( this-object:className,
                         "HandlePut":U, "ClientNumber,cOwnerClientNumber,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("SequenceNumber,&1":U, poRequest:GetPathParameter( "SequenceNumber":U ))
                   cRequiredParamValue[3] = substitute("OwnerClientNumber,&1":U, poRequest:GetPathParameter( "OwnerClientNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber    = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber  = integer(entry(2,cRequiredParamValue[2]))
                   nOwnerClientNumber = integer(entry(2,cRequiredParamValue[3])).

            if nClientNumber > 0 and nOwnerClientNumber > 0 then
            do:
                oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientUnderlyingOwnerWorkflow":U),
                                    com.sit.obp.api.ClientUnderlyingOwnerWorkflow).
                oRequest = cast( poRequest:Entity, JsonObject ).
                if not oRequest:Has( "ClientNumber":U ) then
                    oRequest:Add( "ClientNumber":U, string(nClientNumber) ).
                if not oRequest:Has( "OwnerClientNumber":U ) then
                    oRequest:Add( "OwnerClientNumber":U, string(nOwnerClientNumber) ).
                if not oRequest:Has( "SequenceNumber":U ) then
                    oRequest:Add( "SequenceNumber":U, string(nSequenceNumber) ).


                oWorkflow:Update(input nClientNumber,
                                 input nOwnerClientNumber,
                                 input nSequenceNumber,
                                 input oRequest).
                oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        com.sit.CommonMessageLogger:OperationEnd( this-object:className, "HandlePut":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            com.sit.CommonMessageLogger:OperationException( this-object:className,
                                                  "HandlePut":U,
                                                  new com.sit.exceptions.AppError( oErr ) ).
        end catch.
    end method.

    method override protected integer HandleDelete( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.

        define variable nClientNumber as integer no-undo.
        define variable nOwnerClientNumber as integer no-undo.
        define variable nSequenceNumber as integer no-undo.
        define variable oWorkflow as com.sit.obp.api.ClientUnderlyingOwnerWorkflow no-undo.
        define variable cRequiredParamValue as character extent 3 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleDelete":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse() .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleDelete":U, "ClientNumber,OwnerClientNumber,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("SequenceNumber,&1":U, poRequest:GetPathParameter( "SequenceNumber":U ))
                   cRequiredParamValue[3] = substitute("OwnerClientNumber,&1":U, poRequest:GetPathParameter( "OwnerClientNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber    = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber  = integer(entry(2,cRequiredParamValue[2]))
                   nOwnerClientNumber = integer(entry(2,cRequiredParamValue[3])).

            if nClientNumber > 0 and nOwnerClientNumber > 0 then
            do:
                oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientUnderlyingOwnerWorkflow":U),
                                    com.sit.obp.api.ClientUnderlyingOwnerWorkflow).

                oWorkflow:Delete( input nClientNumber, input nOwnerClientNumber,
                                  input nSequenceNumber ).

                /* We return a 204 here because the request provided has caused the successful deletion of the record.
                   This means that there's nothing to return to the client application.
                   Using HTTP 204 tells the client that this is the case. */
                oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleDelete":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleDelete":U, oErr ).
        end catch.
    end method.
end class.
