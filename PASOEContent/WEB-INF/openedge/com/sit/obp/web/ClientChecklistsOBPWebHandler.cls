
 /*------------------------------------------------------------------------
    File        : ClientChecklistsOBPWebHandler
    Purpose     : Called by PAS to handle api calls for client checklists
    Syntax      :
    Description :
    Author(s)   : lauries
    Created     : Jun 1, 2018
    Notes       :
  ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.*.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.serializer.*.
using com.sit.CommonMessageLogger.

block-level on error undo, throw.

class com.sit.obp.web.ClientChecklistsOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( input poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ),"Character":U) > 0 then do:
            if length(poRequest:GetPathParameter( "ChecklistType":U ),"Character":U) > 0 then do:
                if length(poRequest:GetPathParameter( "SequenceNumber":U ),"Character":U) > 0 then
                    return "GET,PATCH,DELETE":U.
                else
                    return "GET":U.
            end.
            else do:
                return "GET,POST":U.
            end.
        end.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle query request for client checklist items
    Description:
    Notes:
    @param poRequest
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse         as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oBody                 as JsonArray no-undo.
        define variable nClientNumber         as integer no-undo.
        define variable nSequenceNumber       as integer no-undo.
        define variable cChecklistType        as character no-undo.
        define variable lIncludeDocument      as logical initial false  no-undo.
        define variable dtExpiryDateFilter    as date initial ?         no-undo.
        define variable dtDueDateFilter       as date initial ?         no-undo.
        define variable oWorkflow             as com.sit.obp.api.ClientChecklistWorkflow no-undo.
        define variable oParamList            as Progress.Lang.ParameterList no-undo.
        define variable cCompany              as character no-undo.
        define variable cRequiredParamValue   as character extent 1 no-undo.
        define variable cOptionalParamValue   as character extent 2 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign oHttpResponse    = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               cChecklistType   = poRequest:GetPathParameter( "ChecklistType":U )
               cCompany = dynamic-function( "getSessionProperty":U, "CompanyCode":U )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber,SequenceNumber,ChecklistType":U, poRequest ).

        if poRequest:URI:QueryString > "":U then do:
            CommonMessageLogger:LogMessage( this-object:className,
                                            "HandleGet":U,
                                            substitute( "Query Parameters - &1":U, poRequest:URI:QueryString ),
                                            com.sit.logging.Logger:Verbose ).

            lIncludeDocument = com.sit.obp.web.OBPWebHandler:GetLogicalQueryParamValue( "IncludeDocument":U, poRequest ).

            if com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "ExpiryDateFilter":U, poRequest ) <> ? then
            do on error undo, throw:
                dtExpiryDateFilter = com.sit.util.DateType:AsDate(com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "ExpiryDateFilter":U, poRequest )):Value.

                catch oNull as com.sit.exceptions.ArgumentNullException:
                    /* If we catch the oNull exception it's because the date was empty, which should be represented by an unknown; update the variable and move on */
                    dtExpiryDateFilter = ?.
                end catch.

                catch oError as Progress.Lang.Error:
                    /* Re-throw BusinessRuleException with comment if date conversion fails */
                    undo, throw new com.sit.obp.exceptions.BusinessRuleException(substitute("Invalid value for Expiry Date [&1]",com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "ExpiryDateFilter":U, poRequest ))).
                end catch.
            end.

            if com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "DueDateFilter":U, poRequest ) <> ? then
            do on error undo, throw:
                dtDueDateFilter = com.sit.util.DateType:AsDate(com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "DueDateFilter":U, poRequest )):Value.

                catch oNull as com.sit.exceptions.ArgumentNullException:
                    /* If we catch the oNull exception it's because the date was empty, which should be represented by an unknown; update the variable and move on */
                    dtDueDateFilter = ?.
                end catch.

                catch oError as Progress.Lang.Error:
                    /* Re-throw BusinessRuleException with comment if date conversion fails */
                    undo, throw new com.sit.obp.exceptions.BusinessRuleException(substitute("Invalid value for Due Date [&1]",com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "DueDateFilter":U, poRequest ))).
                end catch.
            end.
        end.
        CommonMessageLogger:LogMessage( this-object:className,
                                        "HandleGet":U,
                                        substitute( "lIncludeDocument - &1, dtExpiryDateFilter - &2, dtDueDateFilter - &3":U, lIncludeDocument, dtExpiryDateFilter, dtDueDateFilter ),
                                        com.sit.logging.Logger:Verbose ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cOptionalParamValue[1] = substitute("SequenceNumber,&1":U, poRequest:GetPathParameter( "SequenceNumber":U ))
                   cOptionalParamValue[2] = substitute("ChecklistType,&1":U, cChecklistType).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber  = integer(entry(2,cOptionalParamValue[1])).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(1).
                oParamList:SetParameter(1, "Character":U, "input":U, cCompany).
                oWorkflow = cast(com.sit.obp.ObjectFactory:New(get-class(com.sit.obp.api.ClientChecklistWorkflow), oParamList),
                                  com.sit.obp.api.ClientChecklistWorkflow).

                // GetPathParameter returns empty string when parameter is missing
                if cChecklistType > "":U and entry(2,cOptionalParamValue[1]) > "":U then do:
                    /* getting one unique checklist item */
                    oBody = oWorkflow:Query( nClientNumber, cChecklistType, nSequenceNumber, lIncludeDocument, ?, ?).
                    if not valid-object(oBody) or oBody:Length <> 1 then
                        oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
                    else
                        oHttpResponse:Entity = oBody:GetJsonObject( 1 ).
                end.
                else do:
                    /* get a collection of checklist items */
                    if cChecklistType > "":U then
                        oBody = oWorkflow:Query( nClientNumber, cChecklistType, dtExpiryDateFilter, dtDueDateFilter, lIncludeDocument ).
                    else if dtExpiryDateFilter <> ? or dtDueDateFilter <> ? then
                        oBody = oWorkflow:Query( nClientNumber, lIncludeDocument, dtExpiryDateFilter, dtDueDateFilter ).
                    else
                        oBody = oWorkflow:Query( nClientNumber, lIncludeDocument).
                    if not valid-object( oBody ) then
                        oBody = new JsonArray().
                    oHttpResponse:Entity = oBody.
                end.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        /* catch the validation error message and return to user */
        catch oLogicalTypeValidateErr as com.sit.obp.exceptions.BusinessRuleException :
            oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oLogicalTypeValidateErr ).
            com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).
        end catch.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle add request for client checklist items
    Description:
    Notes:
    @param poRequest
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oRequest      as JsonObject no-undo.
        define variable oResponse     as JsonObject no-undo.
        define variable nClientNumber as integer no-undo.
        define variable oWorkflow     as com.sit.obp.api.ClientChecklistWorkflow no-undo.
        define variable oParamList    as Progress.Lang.ParameterList no-undo.
        define variable cCompany      as character no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:Created)
               cCompany      = dynamic-function( "getSessionProperty":U, "CompanyCode":U )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then
            do:
                oRequest = cast( poRequest:Entity, JsonObject ).

                if not oRequest:Has( "ClientNumber":U ) then
                    oRequest:Add( "ClientNumber":U, entry(2,cRequiredParamValue[1]) ).
                oParamList = new Progress.Lang.ParameterList(1).
                oParamList:SetParameter(1, "Character":U, "input":U, cCompany).
                oWorkflow = cast(com.sit.obp.ObjectFactory:New(get-class(com.sit.obp.api.ClientChecklistWorkflow), oParamList),
                                  com.sit.obp.api.ClientChecklistWorkflow).
                oResponse = oWorkflow:Create(input nClientNumber, input oRequest).
                if valid-object( oResponse ) then
                    oHttpResponse:Entity = oResponse.
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePost":U, oErr ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handleupdate request for client checklist items
    Description:
    Notes:
    @param poRequest
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandlePatch( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse   as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oRequest        as JsonObject no-undo.
        define variable nClientNumber   as integer no-undo.
        define variable cChecklistType  as character no-undo.
        define variable nSequenceNumber as integer no-undo.
        define variable oWorkflow       as com.sit.obp.api.ClientChecklistWorkflow no-undo.
        define variable oParamList      as Progress.Lang.ParameterList no-undo.
        define variable cCompany        as character no-undo.
        define variable cRequiredParamValue as character extent 3 no-undo.
        define variable cOptionalParamValue as character extent no-undo.

        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandlePatch":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:NoContent )
               cChecklistType   = poRequest:GetPathParameter( "ChecklistType":U )
               cCompany         = dynamic-function( "getSessionProperty":U, "CompanyCode":U )
        .

        com.sit.CommonMessageLogger:LogPathParameters( this-object:className,
                         "HandlePatch":U, "ClientNumber,ChecklistType,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U ))
                   cRequiredParamValue[3] = substitute("ChecklistType,&1":U,cChecklistType).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber    = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber  = integer(entry(2,cRequiredParamValue[2])).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(1).
                oParamList:SetParameter(1, "Character":U, "input":U, cCompany).
                oWorkflow = cast(com.sit.obp.ObjectFactory:New(get-class(com.sit.obp.api.ClientChecklistWorkflow), oParamList),
                                 com.sit.obp.api.ClientChecklistWorkflow).
                oRequest = cast( poRequest:Entity, JsonObject ).
                if not oRequest:Has( "ClientNumber":U ) then
                    oRequest:Add( "ClientNumber":U, entry(2,cRequiredParamValue[1]) ).
                if not oRequest:Has( "SequenceNumber":U ) then
                    oRequest:Add( "SequenceNumber":U, entry(2,cRequiredParamValue[2]) ).
                if not oRequest:Has( "ChecklistType":U ) then
                    oRequest:Add( "ChecklistType":U, cChecklistType ).
                oWorkflow:Update(input nClientNumber,
                                 input cChecklistType,
                                 input nSequenceNumber,
                                 input oRequest).
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

         com.sit.CommonMessageLogger:OperationEnd( this-object:className, "HandlePatch":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            com.sit.CommonMessageLogger:OperationException( this-object:className,
                                                  "HandlePatch":U,
                                                  new com.sit.exceptions.AppError( oErr ) ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle delete request for client checklist items
    Description:
    Notes:
    @param poRequest
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandleDelete( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable nClientNumber   as integer no-undo.
        define variable cChecklistType  as character no-undo.
        define variable nSequenceNumber as integer no-undo.
        define variable oWorkflow       as com.sit.obp.api.ClientChecklistWorkflow no-undo.
        define variable oParamList      as Progress.Lang.ParameterList no-undo.
        define variable cCompany        as character no-undo.
        define variable cRequiredParamValue as character extent 3 no-undo.
        define variable cOptionalParamValue as character extent no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleDelete":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:NoContent )
               cChecklistType   = poRequest:GetPathParameter( "ChecklistType":U )
               cCompany         = dynamic-function( "getSessionProperty":U, "CompanyCode":U )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleDelete":U, "ClientNumber,ChecklistType,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U ))
                   cRequiredParamValue[3] = substitute("ChecklistType,&1":U,cChecklistType).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber    = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber  = integer(entry(2,cRequiredParamValue[2])).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(1).
                oParamList:SetParameter(1, "Character":U, "input":U, cCompany).
                oWorkflow = cast(com.sit.obp.ObjectFactory:New(get-class(com.sit.obp.api.ClientChecklistWorkflow), oParamList),
                                 com.sit.obp.api.ClientChecklistWorkflow).
                oWorkflow:Delete( input nClientNumber,
                                  input cChecklistType,
                                  input nSequenceNumber ).
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleDelete":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleDelete":U, oErr ).
        end catch.
    end method.
end class.
