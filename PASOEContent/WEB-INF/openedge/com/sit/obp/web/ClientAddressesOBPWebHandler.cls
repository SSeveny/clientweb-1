 /*------------------------------------------------------------------------
    File        : ClientAddressesOBPWebHandler
    Purpose     : Called by PAS to handle client's address reqeust for CRUD operations
    Syntax      :
    Description :
    Author(s)   : danielc
    Created     : Apr 26, 2018
    Notes       :
  ----------------------------------------------------------------------*/

using OpenEdge.Web.IWebRequest from propath.
using OpenEdge.Net.HTTP.StatusCodeEnum from propath.
using Progress.Json.ObjectModel.JsonArray from propath.
using Progress.Json.ObjectModel.JsonObject from propath.
using com.sit.obp.api.ClientAddressWorkflow from propath.
using com.sit.obp.ObjectFactory from propath.
using com.sit.CommonMessageLogger from propath.

block-level on error undo, throw.

class com.sit.obp.web.ClientAddressesOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    define private property Site as character no-undo
       get():
            if this-object:Site = "" then
                this-object:Site = dynamic-function( "getSessionProperty", "SiteCode" ).

            return this-object:Site.
        end get.
        set.

    define private property Company as character no-undo
       get():
            if this-object:Company = "" then
                this-object:Company = dynamic-function( "getSessionProperty", "CompanyCode" ).

            return this-object:Company.
        end get.
        set.

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( input poRequest as IWebRequest ):
        if length( poRequest:GetPathParameter( "ClientNumber" ), "Character" ) > 0 then do:
            if length( poRequest:GetPathParameter( "AddressType" ), "Character" ) > 0 and
               length( poRequest:GetPathParameter( "EffectiveDate" ), "Character" ) > 0 then
                return "GET,PUT,DELETE".
            else
                return "GET,POST".
        end.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle query request for client's addresses
    Description:
    Notes:
    @param poRequest
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJsonArray as JsonArray no-undo.
        define variable nClientNumber as integer no-undo.
        define variable cAddressType as character no-undo.
        define variable dtEffectiveDate as date no-undo.
        define variable oClientAddressWorkflow as ClientAddressWorkflow no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent 2 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet" ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               cAddressType = poRequest:GetPathParameter( "AddressType" ).

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet", "ClientNumber,EffectiveDate,AddressType", poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1",poRequest:GetPathParameter( "ClientNumber" ))
                   cOptionalParamValue[1] = substitute("AddressType,&1", cAddressType)
                   cOptionalParamValue[2] = substitute("EffectiveDate,&1",poRequest:GetPathParameter( "EffectiveDate" )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            assign
                dtEffectiveDate = this-object:ValidateAndConvertDateParam( "EffectiveDate", entry(2,cOptionalParamValue[2]))
                nClientNumber = integer( entry( 2, cRequiredParamValue[1] ) ).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(2).
                oParamList:SetParameter(1, "Character", "input", this-object:Company).
                oParamList:SetParameter(2, "Character", "input", this-object:Site).

                oClientAddressWorkflow =
                    cast( ObjectFactory:New( "com.sit.obp.api.ClientAddressWorkflow" , oParamList),
                                             com.sit.obp.api.ClientAddressWorkflow ).

                if cAddressType > "" and dtEffectiveDate <> ? then
                do:
                    /* Get one address belong to client */
                    oJsonArray = oClientAddressWorkflow:Query(input nClientNumber,
                                                              input dtEffectiveDate,
                                                              input cAddressType).
                    if valid-object(oJsonArray) and oJsonArray:length = 1 then
                        assign oHttpResponse:Entity = oJsonArray:GetJsonObject(1).
                    else
                        oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
                end.
                else do:
                    /* Get all addresses belong to client */
                    oJsonArray = oClientAddressWorkflow:Query(input nClientNumber).

                    if valid-object(oJsonArray) then
                        oHttpResponse:Entity = oJsonArray.
                    else
                        oHttpResponse:Entity = new JsonArray().
                end.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999", oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet", oErr ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle create request for client's addresses
    Description:
    Notes:
    @param poRequest Json object contains address information to be created by the workflow
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable nClientNumber as integer no-undo.
        define variable oClientAddressWorkflow as ClientAddressWorkflow no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost" ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost", "ClientNumber", poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1",poRequest:GetPathParameter( "ClientNumber" )).

            this-object:ValidateParamValue(input cRequiredParamValue).

            nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(2).
                oParamList:SetParameter(1, "Character", "input", this-object:Company).
                oParamList:SetParameter(2, "Character", "input", this-object:Site).

                oClientAddressWorkflow =
                    cast( ObjectFactory:New( "com.sit.obp.api.ClientAddressWorkflow" , oParamList),
                                             com.sit.obp.api.ClientAddressWorkflow ).

                oJson = oClientAddressWorkflow:Create(input nClientNumber, input cast( poRequest:Entity, JsonObject )).

                if valid-object(oJson) then
                    assign oHttpResponse:Entity = oJson
                           oHttpResponse:StatusCode = integer( StatusCodeEnum:Created ).
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999", oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePost", oErr ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle replace request for client's addresses
    Description:
    Notes: If the address identified by the path parameters exists, then we
           will update the existing address with the specified data, otherwise
           we will create the address. Put must be idempotent.
    @param poRequest Json object contains address information to replace specified address by the workflow
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandlePut( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable nClientNumber as integer no-undo.
        define variable cAddressType as character no-undo.
        define variable dtEffectiveDate as date no-undo.
        define variable oClientAddressWorkflow as ClientAddressWorkflow no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable cRequiredParamValue as character extent 3 no-undo.
        define variable oRequestBody as JsonObject no-undo.
        define variable oQueryResp as JsonArray no-undo.
        define variable oAddressModel as com.sit.obp.model.AddressModel no-undo.
        define variable iResponseCode as integer no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePut" ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               cAddressType = poRequest:GetPathParameter( "AddressType" ).

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePut", "ClientNumber,EffectiveDate,AddressType", poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1",poRequest:GetPathParameter( "ClientNumber" ))
                   cRequiredParamValue[2] = substitute("AddressType,&1", cAddressType)
                   cRequiredParamValue[3] = substitute("EffectiveDate,&1",poRequest:GetPathParameter( "EffectiveDate" )).

            this-object:ValidateParamValue(input cRequiredParamValue).
            assign
                dtEffectiveDate = this-object:ValidateAndConvertDateParam( "EffectiveDate", entry(2,cRequiredParamValue[3]))
                nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 and dtEffectiveDate <> ? then
            do:
                oParamList = new Progress.Lang.ParameterList(2).
                oParamList:SetParameter(1, "Character", "input", this-object:Company).
                oParamList:SetParameter(2, "Character", "input", this-object:Site).

                assign oRequestBody = cast( poRequest:Entity, JsonObject )
                       oClientAddressWorkflow =
                            cast( ObjectFactory:New( "com.sit.obp.api.ClientAddressWorkflow" , oParamList),
                                                     com.sit.obp.api.ClientAddressWorkflow )
                       /* call the workflow query to find out if the record already exists */
                       oQueryResp = oClientAddressWorkflow:Query( input nClientNumber,
                                                                  input dtEffectiveDate,
                                                                  input cAddressType )
                       oAddressModel = cast( ObjectFactory:New( "com.sit.obp.model.AddressModel" ),
                                             com.sit.obp.model.AddressModel)
                .

                if not valid-object(oQueryResp) or oQueryResp:length = 0 then do:

                    /* the record did not exist so we will do a create - first make sure that the
                       request body has the effective date and address type from the path */
                    if not oRequestBody:Has( oAddressModel:EffectiveDate ) then
                        oRequestBody:Add( oAddressModel:EffectiveDate, dtEffectiveDate ).
                    else
                        oRequestBody:Set( oAddressModel:EffectiveDate, dtEffectiveDate ).
                    if not oRequestBody:Has( oAddressModel:AddressType ) then
                        oRequestBody:Add( oAddressModel:AddressType, cAddressType ).
                    else
                        oRequestBody:Set( oAddressModel:AddressType, cAddressType).

                    oJson = oClientAddressWorkflow:Create(input nClientNumber,
                                                          input oRequestBody).
                    iResponseCode = integer( StatusCodeEnum:Created ).
                end.
                else do:
                    /* the record exists...so lets attempt an update */
                    /* if there was not an EffectiveDate in the request, then add it to the request
                       using pdEffectiveDate...this means they are not changing the date, but the
                       validation code is expecting to get a date */
                    if not oRequestBody:Has( oAddressModel:EffectiveDate ) then
                        oRequestBody:Add( oAddressModel:EffectiveDate, dtEffectiveDate ).

                    oJson = oClientAddressWorkflow:Replace(input nClientNumber,
                                                           input dtEffectiveDate,
                                                           input cAddressType,
                                                           input oRequestBody).
                    iResponseCode = integer( StatusCodeEnum:NoContent ).
                end.

                if valid-object(oJson) then
                    /* In the case we did a create, we will return a 201. In the case of an update,
                       we return a 204 here because the data provided to this method has successfully overwritten the record.
                       This means that client application already has the current version of it, we don't need to send it back.
                       Using HTTP 204 tells the client that this is the case. */
                    oHttpResponse:StatusCode = iResponseCode.
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePut" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999", oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePut", oErr ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle delete request for client's addresses
    Description:
    Notes:
    @param poRequest Json object contains address to be deleted by the workflow
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandleDelete( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable nClientNumber as integer no-undo.
        define variable cAddressType as character no-undo.
        define variable dtEffectiveDate as date no-undo.
        define variable oClientAddressWorkflow as ClientAddressWorkflow no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable cRequiredParamValue as character extent 3 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleDelete" ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               cAddressType = poRequest:GetPathParameter( "AddressType" ).

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleDelete", "ClientNumber,EffectiveDate,AddressType", poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1",poRequest:GetPathParameter( "ClientNumber" ))
                   cRequiredParamValue[2] = substitute("AddressType,&1", cAddressType)
                   cRequiredParamValue[3] = substitute("EffectiveDate,&1",poRequest:GetPathParameter( "EffectiveDate" )).

            this-object:ValidateParamValue(input cRequiredParamValue).

            assign
                dtEffectiveDate = this-object:ValidateAndConvertDateParam( "EffectiveDate", entry(2,cRequiredParamValue[3]))
                nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 and dtEffectiveDate <> ? then
            do:
                oParamList = new Progress.Lang.ParameterList(2).
                oParamList:SetParameter(1, "Character", "input", this-object:Company).
                oParamList:SetParameter(2, "Character", "input", this-object:Site).

                oClientAddressWorkflow =
                    cast( ObjectFactory:New( "com.sit.obp.api.ClientAddressWorkflow" , oParamList),
                                             com.sit.obp.api.ClientAddressWorkflow ).

                oClientAddressWorkflow:Delete(input nClientNumber,
                                              input dtEffectiveDate,
                                              input cAddressType).

                /* We return a 204 here because the request provided has caused the successful deletion of the record.
                   This means that there's nothing to return to the client application.
                   Using HTTP 204 tells the client that this is the case. */
                oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationStart( this-object:className, "HandleDelete" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999", oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleDelete", oErr ).
        end catch.
    end method.
end class.