
/*------------------------------------------------------------------------
   File        : ClientPasswordOBPWebHandler
   Purpose     :
   Syntax      :
   Description :
   Author(s)   : bennettb
   Created     : Wed Aug 22 10:31:53 EDT 2018
   Notes       :
 ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.WebResponseWriter.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.CommonMessageLogger.
using OpenEdge.Web.*.
using com.sit.obp.api.ClientPasswordWorkflow from propath.

block-level on error undo, throw.

class com.sit.obp.web.ClientPasswordOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( input poRequest as IWebRequest ):
        return "POST":U.
    end method.

    /*------------------------------------------------------------------------------
            Purpose: POST requests for the /Clients/{ClientNumber}/Password API endpoint will perform Client Password Updates
            Notes: This is not a PUT because the changing password history
    ------------------------------------------------------------------------------*/
    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse          as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oRequestBody           as JsonObject                      no-undo.
        define variable nClientNumberPathParam as integer                         no-undo.
        define variable cInterfaceSessionValue as character                       no-undo.
        define variable oClientPasswordWorkflow as ClientPasswordWorkflow         no-undo.
        define variable cRequiredParamValue as character extent 1                 no-undo.
        define variable cOptionalParamValue as character extent                   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U ).

        assign
            oHttpResponse          = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:NoContent )
            oRequestBody           = cast( poRequest:Entity, JsonObject )
            cInterfaceSessionValue = dynamic-function( "getSessionProperty":U, "OBPInterfaceCode":U )
            .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost":U, "ClientNumber":U, poRequest ).

        if valid-object( oRequestBody ) then
        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            assign nClientNumberPathParam = integer(entry(2,cRequiredParamValue[1])).
            
            if nClientNumberPathParam > 0 then
            do: 
                oClientPasswordWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientPasswordWorkflow":U),
                                           com.sit.obp.api.ClientPasswordWorkflow).
            
                oClientPasswordWorkflow:SetPassword(cInterfaceSessionValue, nClientNumberPathParam, oRequestBody).
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
            
            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.
        else
            oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).
        
        this-object:setHttpResponse(oHttpResponse).
        
        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            this-object:agentLogger:Error( new com.sit.exceptions.AppError( oErr ) ).

        /* TODO: Need to turn this into a proper error response that gets returned */
        end catch.
    end method.

end class.
