
 /*------------------------------------------------------------------------
    File        : ClientRegulatoryOBPWebHandler
    Purpose     :
    Syntax      :
    Description :
    Author(s)   : shainep
    Created     : May 28, 2018
    Notes       :
  ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.*.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.serializer.*.
using com.sit.CommonMessageLogger.
using com.sit.obp.api.ClientRegulatory.

block-level on error undo, throw.

class com.sit.obp.web.ClientRegulatoryOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ), "Character":U) > 0 then
            return "GET,PUT":U.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable nClientPathParam as integer no-undo.
        define variable oRegulatoryWorkflow as ClientRegulatory no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            assign nClientPathParam = integer(entry(2,cRequiredParamValue[1])).
            
            if nClientPathParam > 0 then
            do:
                oRegulatoryWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientRegulatory":U),com.sit.obp.api.ClientRegulatory).
            
                oJson = oRegulatoryWorkflow:Query(input nClientPathParam). /* Error may happen here? How to manage this? */

                if valid-object(oJson) then
                    assign oHttpResponse:Entity = oJson.
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

    method override protected integer HandlePut( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse   as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson           as JsonObject no-undo.
        define variable nClientNumber   as integer no-undo.
        define variable oRegulatoryWorkflow as ClientRegulatory no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        assign oHttpResponse     = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandlePut":U ).

        com.sit.CommonMessageLogger:LogPathParameters( this-object:className, "HandlePut":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            assign nClientNumber = integer(entry(2,cRequiredParamValue[1])).
            
            if nClientNumber > 0 then 
            do:
                oRegulatoryWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientRegulatory":U),com.sit.obp.api.ClientRegulatory).
                oJson = cast( poRequest:Entity, JsonObject ).
                if not oJson:Has( "ClientNumber":U ) then
                    oJson:Add( "ClientNumber":U, nClientNumber ).
                oRegulatoryWorkflow:Replace(input nClientNumber, input oJson).
                oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
            
            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        com.sit.CommonMessageLogger:OperationEnd( this-object:className, "HandlePut":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            com.sit.CommonMessageLogger:OperationException( this-object:className, "HandlePut":U, new com.sit.exceptions.AppError( oErr ) ).
        end catch.
    end method.
end class.