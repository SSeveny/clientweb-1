
/*------------------------------------------------------------------------
   File        : ClientChecklistsOBPWebHandler
   Purpose     : Called by PAS to handle api calls for client checklists
   Syntax      :
   Description :
   Author(s)   : lauries
   Created     : Jun 1, 2018
   Notes       :
 ----------------------------------------------------------------------*/

using OpenEdge.Net.HTTP.StatusCodeEnum.
using com.sit.CommonMessageLogger.
using OpenEdge.Web.IWebRequest from propath.
using Progress.Json.ObjectModel.JsonArray from propath.
using Progress.Json.ObjectModel.JsonObject from propath.
using com.sit.obp.api.ClientImageWorkflow from propath.
using com.sit.obp.config.ApplicationConfiguration from propath.

block-level on error undo, throw.

class com.sit.obp.web.ClientImageOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    define private property oWorkflow as ClientImageWorkflow no-undo
        get():
            define variable oParamList as Progress.Lang.ParameterList no-undo.
            define variable cCompany   as character                   no-undo.
            if not valid-object(this-object:oWorkflow) then
            do:
                cCompany = ApplicationConfiguration:GetValue( "CompanyCode":U ).

                oParamList = new Progress.Lang.ParameterList(1).
                oParamList:SetParameter(1, "Character":U, "input":U, cCompany).
                oWorkflow = cast(com.sit.obp.ObjectFactory:New(get-class(ClientImageWorkflow), oParamList),
                    ClientImageWorkflow).
            end.
            return this-object:oWorkflow.
        end get.
        set.

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( input poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ),"Character":U) > 0 then
        do:
            return "GET,PUT":U.
        end.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

    /*------------------------------------------------------------------------
     Purpose:  Handle query request for client checklist items
     Description:
     Notes:
     @param poRequest
     @return Http status number
     ------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse       as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oBody               as JsonObject                      no-undo.
        define variable nClientNumber       as integer                         no-undo.
        define variable cRequiredParamValue as character                       extent 1 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign
            oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
            .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign
                cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue).

            assign
                nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then
            do:
                oBody = this-object:oWorkflow:Query(nClientNumber).
                oHttpResponse:Entity = oBody.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

    /*------------------------------------------------------------------------
     Purpose:  Handleupdate request for client Images
     Description:
     Notes:
     @param poRequest
     @return Http status number
     ------------------------------------------------------------------------*/
    method override protected integer HandlePut( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse       as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oRequest            as JsonObject                      no-undo.
        define variable oBody               as JsonObject                      no-undo.
        define variable nClientNumber       as integer                         no-undo.
        define variable cRequiredParamValue as character                       extent 1 no-undo.

        com.sit.CommonMessageLogger:OperationStart( this-object:className, "HandlePut":U ).

        assign
            oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:NoContent )
            .

        com.sit.CommonMessageLogger:LogPathParameters( this-object:className,
            "HandlePut":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign
                cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue).

            assign
                nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then
            do:
                oRequest = cast( poRequest:Entity, JsonObject ).
                oBody = this-object:oWorkflow:Update(nClientNumber, oRequest).
                oHttpResponse:Entity = oBody.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        com.sit.CommonMessageLogger:OperationEnd( this-object:className, "HandlePut":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            com.sit.CommonMessageLogger:OperationException( this-object:className,
                "HandlePut":U,
                new com.sit.exceptions.AppError( oErr ) ).
        end catch.
    end method.
end class.
