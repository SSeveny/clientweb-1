
/*------------------------------------------------------------------------
   File        : ClientPasswordResetOBPWebHandler
   Purpose     :
   Syntax      :
   Description :
   Author(s)   : bennettb
   Created     : Wed Aug 22 10:31:53 EDT 2018
   Notes       :
 ----------------------------------------------------------------------*/

using OpenEdge.Web.WebResponseWriter from propath.
using OpenEdge.Web.IWebRequest from propath.
using OpenEdge.Net.HTTP.StatusCodeEnum from propath.
using Progress.Json.ObjectModel.JsonObject from propath.
using com.sit.CommonMessageLogger from propath.
using com.sit.obp.api.PasswordResetWorkflow from propath.
using com.sit.obp.web.OBPWebHandler from propath.
using com.sit.obp.web.OBPDefaultWebResponse from propath.
using com.sit.obp.config.ApplicationConfiguration from propath.

block-level on error undo, throw.

class com.sit.obp.web.ClientPasswordResetOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( input poRequest as IWebRequest ):
        define variable cGetString as character no-undo.

        if length(poRequest:GetPathParameter( "ResetToken":U ), "Character":U) > 0 then
            cGetString = "GET,PATCH,":U.

        return cGetString + "POST":U.
    end method.

    /*------------------------------------------------------------------------------
            Purpose: POST requests for the /Clients/Password[/{ResetToken}] API endpoints will initiate a password reset, or set a new password
            Notes: This is not a PUT because the changing password history
    ------------------------------------------------------------------------------*/
    method override protected integer HandlePatch( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse            as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oRequestBody             as JsonObject                      no-undo.
        define variable cResetTokenPathParameter as character                       no-undo.
        define variable cSubSystemSessionParam   as character                       no-undo.
        define variable oPasswordResetWorkflow   as PasswordResetWorkflow           no-undo.
        define variable cRequiredParamValue as character extent 1                   no-undo.
        define variable cOptionalParamValue as character extent                     no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePatch":U ).

        assign
            oHttpResponse            = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:NoContent )
            oRequestBody             = cast( poRequest:Entity, JsonObject )
            cResetTokenPathParameter = poRequest:GetPathParameter( "ResetToken":U )
            cSubSystemSessionParam   = ApplicationConfiguration:KeyValueConfiguration:GetValue( "OBPInterfaceCode":U )
            .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePatch":U, "ResetToken":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ResetToken,&1":U,cResetTokenPathParameter).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            if valid-object( oRequestBody ) then
            do:
                oPasswordResetWorkflow   = cast( com.sit.obp.ObjectFactory:New( get-class( PasswordResetWorkflow ) ),
                                             PasswordResetWorkflow ).
                oPasswordResetWorkflow:SetNewPassword(cSubSystemSessionParam, cResetTokenPathParameter, oRequestBody).
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).
        
        this-object:setHttpResponse(oHttpResponse).
        
        CommonMessageLogger:OperationEnd( this-object:className, "HandlePatch":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            this-object:agentLogger:Error( new com.sit.exceptions.AppError( oErr ) ).

        /* TODO: Need to turn this into a proper error response that gets returned */
        end catch.
        finally:
            oPasswordResetWorkflow:DebugMode = false.
        end finally.
    end method.

    /*------------------------------------------------------------------------------
            Purpose: POST requests for the /Clients/Password[/{ResetToken}] API endpoints will initiate a password reset, or set a new password
            Notes: This is not a PUT because the changing password history
    ------------------------------------------------------------------------------*/
    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse            as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oRequestBody             as JsonObject                      no-undo.
        define variable oBody                    as JsonObject                      no-undo.
        define variable cResetTokenPathParameter as character                       no-undo.
        define variable cAlertMessage            as character                       no-undo.
        define variable oPasswordResetWorkflow   as PasswordResetWorkflow           no-undo.
        define variable cRequiredParamValue as character extent                     no-undo.
        define variable cOptionalParamValue as character extent 1                   no-undo.
        define variable cDebugMode as character no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U ).

        assign
            oHttpResponse            = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:NoContent )
            oRequestBody             = cast( poRequest:Entity, JsonObject )
            cResetTokenPathParameter = poRequest:GetPathParameter( "ResetToken":U )
            oPasswordResetWorkflow   = cast( com.sit.obp.ObjectFactory:New( get-class( PasswordResetWorkflow ) ),
                                             PasswordResetWorkflow )
            cDebugMode = ApplicationConfiguration:KeyValueConfiguration:GetValue( "DebugMode":U )
            oPasswordResetWorkflow:DebugMode = ( cDebugMode <> ? and cDebugMode = "ON":U )
            .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost":U, "ResetToken":U, poRequest ).

        if valid-object( oRequestBody ) then
        do on error undo, throw:
            assign cOptionalParamValue[1] = substitute("ResetToken,&1":U,cResetTokenPathParameter).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            if cResetTokenPathParameter = "":U then
            do:
                cAlertMessage = oPasswordResetWorkflow:InitiatePasswordReset(oRequestBody:GetCharacter("Username":U)).

                if cAlertMessage <> ? then
                do:
                    oBody = new JsonObject().
                    oBody:Add("ResponseMessage":U, cAlertMessage).
                    oHttpResponse = OBPWebHandler:GetDefaultResponse().
                    oHttpResponse:Entity = oBody.
                end.
            end.
            else
            do:
                if oRequestBody:Has("Answer":U) then
                    oPasswordResetWorkflow:ResolveChallenge(cResetTokenPathParameter, oRequestBody).
            end.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.
        else
            oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).
        
        this-object:setHttpResponse(oHttpResponse).
        
        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            this-object:agentLogger:Error( new com.sit.exceptions.AppError( oErr ) ).

        /* TODO: Need to turn this into a proper error response that gets returned */
        end catch.
        finally:
            oPasswordResetWorkflow:DebugMode = false.
        end finally.
    end method.

    /*------------------------------------------------------------------------------
            Purpose: GET request for the /Clients/Password/{ResetToken} API endpoint will return a reset challenge question.
            Notes: It will return the same question until it's correctly answered.
    ------------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse            as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oBody                    as JsonObject                      no-undo.
        define variable cResetTokenPathParameter as character                       no-undo.
        define variable oPasswordResetWorkflow   as PasswordResetWorkflow           no-undo.
        define variable cRequiredParamValue as character extent 1                   no-undo.
        define variable cOptionalParamValue as character extent                     no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign
            oHttpResponse            = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
            cResetTokenPathParameter = poRequest:GetPathParameter( "ResetToken":U )
            oPasswordResetWorkflow   = cast( com.sit.obp.ObjectFactory:New( get-class( PasswordResetWorkflow ) ),
                                             PasswordResetWorkflow )
            .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ResetToken":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ResetToken,&1":U,cResetTokenPathParameter).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            oBody = oPasswordResetWorkflow:LoadResetChallenge(cResetTokenPathParameter).
            oHttpResponse:Entity = oBody.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).
        
        this-object:setHttpResponse(oHttpResponse).
        
        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            this-object:agentLogger:Error( new com.sit.exceptions.AppError( oErr ) ).

        /* TODO: Need to turn this into a proper error response that gets returned */
        end catch.
        finally:
            oPasswordResetWorkflow:DebugMode = false.
        end finally.
    end method.

end class.
