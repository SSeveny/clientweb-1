
 /*------------------------------------------------------------------------
    File        : ClientPayeesOBPWebHandler
    Purpose     :
    Syntax      :
    Description :
    Author(s)   : chrisr
    Created     : Jul 16, 2018 12:38:04 PM
    Notes       :
  ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.*.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.serializer.*.
using com.sit.CommonMessageLogger.
using com.sit.obp.web.OBPDefaultWebResponse.

block-level on error undo, throw.

class com.sit.obp.web.ClientPayeesOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

   /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( input poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ), "Character":U) > 0 then
            if length(poRequest:GetPathParameter( "SequenceNumber":U ), "Character":U) > 0 then
                return "GET,PATCH,DELETE":U.
            else
                return "GET,POST":U.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oArray as JsonArray no-undo.
        define variable dClientNumberPathParameter as decimal no-undo.
        define variable cCompanyPathParameter as character no-undo.
        define variable nSequenceNumber as integer no-undo.
        define variable oWorkflow as com.sit.obp.api.ClientBillVendorsWorkflow no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent 1 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               cCompanyPathParameter = poRequest:GetPathParameter( "Company":U )
               oWorkflow = cast( com.sit.obp.ObjectFactory:New( "com.sit.obp.api.ClientBillVendorsWorkflow":U ), com.sit.obp.api.ClientBillVendorsWorkflow )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber,Company":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("Company,&1":U,cCompanyPathParameter)
                   cOptionalParamValue[1] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign dClientNumberPathParameter = decimal(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber = integer(entry(2,cOptionalParamValue[1])).

            /* If the core path parameter is present, we should be returning a single resource representation. Otherwise, a collection has been requested */
            if dClientNumberPathParameter > 0 then 
            do:
                // GetPathParameter returns empty string when parameter is missing
                if entry(2,cOptionalParamValue[1]) > "":U then
                do:
                    oArray = oWorkflow:Query( dClientNumberPathParameter, input nSequenceNumber, cCompanyPathParameter ).

                    if valid-object(oArray) and oArray:length = 1 then
                        oHttpResponse:Entity = oArray:GetJsonObject(1).
                    else
                        oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
                end.
                else do:
                    oArray = oWorkflow:Query( dClientNumberPathParameter, cCompanyPathParameter ).

                    if valid-object(oArray) then
                        oHttpResponse:Entity = oArray.
                    else
                        oHttpResponse:Entity = new JsonArray().
                end.
            end.
            else
                oHttpResponse = OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable dClientNumber as decimal no-undo.
        define variable cCompany as character no-undo.
        define variable oRequestBody as JsonObject no-undo.
        define variable oResponse as JsonObject no-undo.
        define variable oWorkflow as com.sit.obp.api.ClientBillVendorsWorkflow no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:Created )
               cCompany = poRequest:GetPathParameter( "Company":U )
               oRequestBody = cast( poRequest:Entity, JsonObject )
               oWorkflow = cast( com.sit.obp.ObjectFactory:New( "com.sit.obp.api.ClientBillVendorsWorkflow":U ), com.sit.obp.api.ClientBillVendorsWorkflow )
        .
        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost":U, "ClientNumber,Company":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("Company,&1":U,cCompany).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign dClientNumber = decimal(entry(2,cRequiredParamValue[1])).

            if dClientNumber > 0 then 
            do:
                oResponse = oWorkflow:Create( input dClientNumber,
                                              input cCompany,
                                              input oRequestBody ).
                if valid-object( oResponse ) then
                    assign oHttpResponse:Entity = oResponse
                    .
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
            end.
            else
              oHttpResponse = OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.
        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePost":U, oErr ).
        end catch.
    end method.

    method override protected integer HandlePatch( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oRequestBody as JsonObject no-undo.
        define variable dClientNumber as decimal no-undo.
        define variable cCompany as character no-undo.
        define variable nSequenceNumber as integer no-undo.
        define variable oWorkflow as com.sit.obp.api.ClientBillVendorsWorkflow no-undo.
        define variable cRequiredParamValue as character extent 3 no-undo.
        define variable cOptionalParamValue as character extent no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePatch":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               oRequestBody = cast( poRequest:Entity, JsonObject )
               cCompany = poRequest:GetPathParameter( "Company":U )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePatch":U, "ClientNumber,Company,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("Company,&1":U,cCompany)
                   cRequiredParamValue[3] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign dClientNumber = decimal(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber = integer(entry(2,cRequiredParamValue[3])).

            if dClientNumber > 0 then
            do:
                oWorkflow = cast( com.sit.obp.ObjectFactory:New( "com.sit.obp.api.ClientBillVendorsWorkflow":U ), com.sit.obp.api.ClientBillVendorsWorkflow ). 
                oHttpResponse:Entity =  oWorkflow:Update( input dClientNumber,
                                                          input nSequenceNumber,
                                                          input cCompany,
                                                          oRequestBody ).
            end.
            else
              oHttpResponse = OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationStart( this-object:className, "HandlePatch":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePatch":U, oErr ).
        end catch.
    end method.

    method override protected integer HandleDelete( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable nClientNumber as integer no-undo.
        define variable cCompany as character no-undo.
        define variable nSequenceNumber as integer no-undo.
        define variable oWorkflow as com.sit.obp.api.ClientBillVendorsWorkflow no-undo.
        define variable cRequiredParamValue as character extent 3 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleDelete":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:NoContent ) /* nothing to return on deletion */
               cCompany = poRequest:GetPathParameter( "Company":U )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleDelete":U, "ClientNumber,Company,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("Company,&1":U,cCompany)
                   cRequiredParamValue[3] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber = integer(entry(2,cRequiredParamValue[3])).

            if nClientNumber > 0 then 
            do:
                oWorkflow = cast( com.sit.obp.ObjectFactory:New( "com.sit.obp.api.ClientBillVendorsWorkflow":U ), com.sit.obp.api.ClientBillVendorsWorkflow ). 
                oWorkflow:Delete( input nClientNumber,
                                  input nSequenceNumber,
                                  input cCompany ).
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationStart( this-object:className, "HandleDelete":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleDelete":U, oErr ).
        end catch.
    end method.
end class.
