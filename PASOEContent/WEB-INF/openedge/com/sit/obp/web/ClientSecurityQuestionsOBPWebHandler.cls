
 /*------------------------------------------------------------------------
    File        : ClientSecurityQuestionsOBPWebHandler
    Purpose     : Called by the PAS to handle API requests for the client
                : security questions
    Syntax      :
    Description :
    Author(s)   : lauries
    Created     : Thu Oct 04 12:01:44 EDT 2018
    Notes       :
  ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.WebResponseWriter.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.CommonMessageLogger.
using OpenEdge.Web.*.

block-level on error undo, throw.

class com.sit.obp.web.ClientSecurityQuestionsOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    constructor public ClientSecurityQuestionsOBPWebHandler():
       /*constructor has no return type*/
    end constructor.

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( input poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ), "Character":U) > 0 and  length(poRequest:GetPathParameter( "Company":U ), "Character":U) > 0 then do:
            if length(poRequest:GetPathParameter( "SequenceNumber":U ), "Character":U) > 0 then
                return "GET,PATCH,DELETE":U.
            return "GET,POST":U.
        end.
        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

    /*------------------------------------------------------------------------
     Purpose:  Handle query request for client security questions
     Description:
     Notes:
     @param poRequest
     @return Http status number
     ------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse   as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oBody           as JsonConstruct                   no-undo.
        define variable nClientNumber   as integer                         no-undo.
        define variable cCompany        as character                       no-undo.
        define variable nSequenceNumber as integer                         no-undo.
        define variable oWorkflow       as com.sit.obp.api.ClientSecurityQuestionsWorkflow no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent 1 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign
            oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
            cCompany      = poRequest:GetPathParameter( "Company":U )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber,Company,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("Company,&1":U,cCompany)
                   cOptionalParamValue[1] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber = integer(entry(2,cOptionalParamValue[1])).

            if nClientNumber > 0 then 
            do:
                oWorkflow = cast(com.sit.obp.ObjectFactory:New(get-class(com.sit.obp.api.ClientSecurityQuestionsWorkflow)),
                                 com.sit.obp.api.ClientSecurityQuestionsWorkflow).
                // GetPathParameter returns empty string when parameter is missing                 
                if entry(2,cOptionalParamValue[1]) > "":U then do:
                    /* getting one unique security question - this query will return a JsonObject*/
                    oBody = oWorkflow:Query( cCompany, nClientNumber, nSequenceNumber ).
                    if not valid-object(oBody) then
                        oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
                    else
                        oHttpResponse:Entity = oBody.
                end.
                else do:
                    /* get a collection of checklist items */
                    oBody = oWorkflow:Query( cCompany, nClientNumber ).
                    if not valid-object( oBody ) then
                        oBody = new JsonArray().
                    oHttpResponse:Entity = oBody.
                end.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

    /*------------------------------------------------------------------------
     Purpose:  Handle create request for client security questions
     Description:
     Notes:
     @param poRequest
     @return Http status number
     ------------------------------------------------------------------------*/
    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse   as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oResponse     as JsonObject no-undo.
        define variable nClientNumber   as integer                         no-undo.
        define variable cCompany        as character                       no-undo.
        define variable oWorkflow       as com.sit.obp.api.ClientSecurityQuestionsWorkflow no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U ).

        assign
            oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:Created )
            cCompany      = poRequest:GetPathParameter( "Company":U )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost":U, "ClientNumber,Company":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("Company,&1":U,cCompany).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then 
            do:
                oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientSecurityQuestionsWorkflow":U),
                                 com.sit.obp.api.ClientSecurityQuestionsWorkflow).
                oResponse = oWorkflow:Create( cCompany, nClientNumber, cast( poRequest:Entity, JsonObject ) ).
                if not valid-object(oResponse) then
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
                else
                    oHttpResponse:Entity = oResponse.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePost":U, oErr ).
        end catch.

    end method.

    /*------------------------------------------------------------------------
     Purpose:  Handle update request for client security questions
     Description:
     Notes:
     @param poRequest
     @return Http status number
     ------------------------------------------------------------------------*/
    method override protected integer HandlePatch( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse   as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable nClientNumber   as integer                         no-undo.
        define variable cCompany        as character                       no-undo.
        define variable nSequenceNumber as integer                         no-undo.
        define variable oWorkflow       as com.sit.obp.api.ClientSecurityQuestionsWorkflow no-undo.
        define variable cRequiredParamValue as character extent 3 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePatch":U ).

        assign
            oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:NoContent )
            cCompany      = poRequest:GetPathParameter( "Company":U )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePatch":U, "ClientNumber,Company,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("Company,&1":U,cCompany)
                   cRequiredParamValue[3] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber = integer(entry(2,cRequiredParamValue[3])).

            if nClientNumber > 0 then 
            do:
                oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientSecurityQuestionsWorkflow":U),
                                 com.sit.obp.api.ClientSecurityQuestionsWorkflow).
                oWorkflow:Update( cCompany, nClientNumber, nSequenceNumber, cast( poRequest:Entity, JsonObject ) ).
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        com.sit.CommonMessageLogger:OperationEnd( this-object:className, "HandlePatch":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            com.sit.CommonMessageLogger:OperationException( this-object:className,
                                                  "HandlePatch":U,
                                                  new com.sit.exceptions.AppError( oErr ) ).
        end catch.

    end method.

    /*------------------------------------------------------------------------
     Purpose:  Handle delete request for client security questions
     Description:
     Notes:
     @param poRequest
     @return Http status number
     ------------------------------------------------------------------------*/
    method override protected integer HandleDelete( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse   as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable nClientNumber   as integer                         no-undo.
        define variable cCompany        as character                       no-undo.
        define variable nSequenceNumber as integer                         no-undo.
        define variable oWorkflow       as com.sit.obp.api.ClientSecurityQuestionsWorkflow no-undo.
        define variable cRequiredParamValue as character extent 3 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleDelete":U ).

        assign
            oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse( StatusCodeEnum:NoContent )
            cCompany      = poRequest:GetPathParameter( "Company":U )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleDelete":U, "ClientNumber,Company,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("Company,&1":U,cCompany)
                   cRequiredParamValue[3] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1]))
                   nSequenceNumber = integer(entry(2,cRequiredParamValue[3])).

            if nClientNumber > 0 then
            do:
                oWorkflow = cast(com.sit.obp.ObjectFactory:New("com.sit.obp.api.ClientSecurityQuestionsWorkflow":U),
                                 com.sit.obp.api.ClientSecurityQuestionsWorkflow).
                oWorkflow:Delete( cCompany, input nClientNumber, input nSequenceNumber).
            end.
            /* If ClientNumber, Company or Sequence Number parameters are missing, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleDelete":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleDelete":U, oErr ).
        end catch.
    end method.
end class.